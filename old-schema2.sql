-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mnm
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mnm
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mnm` DEFAULT CHARACTER SET latin1 ;
USE `mnm` ;

-- -----------------------------------------------------
-- Table `mnm`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mnm`.`users` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(45) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `isBanned` TINYINT(4) NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC) VISIBLE)
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `mnm`.`books`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mnm`.`books` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NOT NULL,
  `author` VARCHAR(100) NOT NULL,
  `year` INT(11) NOT NULL,
  `genre` VARCHAR(45) NOT NULL,
  `isBorrowed` VARCHAR(45) NULL DEFAULT '0',
  `users_id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_books_users_idx` (`users_id` ASC) VISIBLE,
  CONSTRAINT `fk_books_users`
    FOREIGN KEY (`users_id`)
    REFERENCES `mnm`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `mnm`.`reviews`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mnm`.`reviews` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `content` VARCHAR(255) NOT NULL,
  `isDeleted` TINYINT(4) NULL DEFAULT 0,
  `books_id` INT(11) NOT NULL,
  `users_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_reviews_books1_idx` (`books_id` ASC) VISIBLE,
  INDEX `fk_reviews_users1_idx` (`users_id` ASC) VISIBLE,
  CONSTRAINT `fk_reviews_books1`
    FOREIGN KEY (`books_id`)
    REFERENCES `mnm`.`books` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_reviews_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `mnm`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `mnm`.`likes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mnm`.`likes` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `like` ENUM('LIKE', 'LOVE', 'HEARTH', 'SAD', 'WOW') NULL DEFAULT NULL,
  `isDeleted` TINYINT(4) NULL DEFAULT 0,
  `reviews_id` INT(11) NOT NULL,
  `users_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_likes_reviews1_idx` (`reviews_id` ASC) VISIBLE,
  INDEX `fk_likes_users1_idx` (`users_id` ASC) VISIBLE,
  CONSTRAINT `fk_likes_reviews1`
    FOREIGN KEY (`reviews_id`)
    REFERENCES `mnm`.`reviews` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_likes_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `mnm`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `mnm`.`ratings`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mnm`.`ratings` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `rating` ENUM('1', '2', '3', '4', '5') NULL DEFAULT NULL,
  `isDeleted` TINYINT(4) NULL DEFAULT 0,
  `books_id` INT(11) NOT NULL,
  `users_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_ratings_books1_idx` (`books_id` ASC) VISIBLE,
  INDEX `fk_ratings_users1_idx` (`users_id` ASC) VISIBLE,
  CONSTRAINT `fk_ratings_books1`
    FOREIGN KEY (`books_id`)
    REFERENCES `mnm`.`books` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ratings_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `mnm`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `mnm`.`readBooksByUsers`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mnm`.`readBooksByUsers` (
  `books_id` INT(11) NOT NULL,
  `users_id` INT(11) NOT NULL,
  INDEX `fk_readBooksByUsers_books1_idx` (`books_id` ASC) VISIBLE,
  INDEX `fk_readBooksByUsers_users1_idx` (`users_id` ASC) VISIBLE,
  CONSTRAINT `fk_readBooksByUsers_books1`
    FOREIGN KEY (`books_id`)
    REFERENCES `mnm`.`books` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_readBooksByUsers_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `mnm`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
