let GameManager = {
    // This resets the arena
    setGameStart(classType) {
        this.resetPlayer(classType);
        this.setPreFight();
    },
    // This generates the different views for the classes
    resetPlayer(classType) {
        switch (classType) {
            case "Warrior":
                player = new Player(classType, 200, 0, 200, 100, 50);
                break;

            case "Assassin":
                player = new Player(classType, 100, 0, 100, 150, 200);
                break;

            case "Mage":
                player = new Player(classType, 80, 0, 50, 200, 50);
                break;

            case "Knight":
                player = new Player(classType, 200, 0, 50, 200, 100);
                break;
        }

        // This shows the player details 

        let getInterface = document.querySelector(".interface")
        getInterface.innerHTML = `<img src="characters/${classType.toLowerCase()}.jpg" class="img-avatar">
        <div>
          <h3>${classType}</h3>
        </div>
        <p id="health-player">Health: ${player.health}</p>
        <p>Mana: ${player.mana}</p>
        <p>Strength: ${player.strength}</p>
        <p>Agility: ${player.agility}</p>
        <p>Speed: ${player.speed}</p>`
    },
    
    // This is the find an enemy section and the search for an opponent button

    setPreFight() {
        let getHeader = document.querySelector(".header")
        let getActions = document.querySelector(".actions")
        let getArena = document.querySelector(".arena")
        getHeader.innerHTML = `<p>Find an enemy!</p>`
        getActions.innerHTML = `<a href="#" class="btn-prefight" onclick="GameManager.setFight()">Search for an enemy!</a>`
        getArena.style.visibility = "hidden";
    },



    setFight() {
        let getHeader = document.querySelector(".header")
        let getActions = document.querySelector(".actions")
        let getEnemy = document.querySelector(".enemy")
        let getArena = document.querySelector(".arena")
        getArena.style.visibility = "visible"
        // This creates an enemy

        let enemy00 = new Enemy("Nashor", 250, 0, 100, 100, 100)
        let enemy01 = new Enemy("Dragon", 150, 0, 50, 150, 50)
        let enemy02 = new Enemy("Krugs", 50, 0, 25, 25, 25)
        let enemy03 = new Enemy("Bramble", 100, 0, 50, 50, 50)
        let enemy04 = new Enemy("Sentinel", 100, 0, 50, 50, 50)
        let enemy05 = new Enemy("Raptors", 50, 0, 25, 50, 50)
        let enemy06 = new Enemy("Wolves", 50, 0, 25, 25, 25)
        let enemy07 = new Enemy("Scuttle", 150, 0, 0, 0, 0)

        let chooseRandomEnemy = Math.floor(Math.random()*8)
        console.log(chooseRandomEnemy)

        switch (chooseRandomEnemy) {

            case 0:
                enemy = enemy00;
                break;

                
            case 1:
                enemy = enemy01;
                break;

                
            case 2:
                enemy = enemy02;
                break;

                
            case 3:
                enemy = enemy03;
                break;

                
            case 4:
                enemy = enemy04;
                break;

                
            case 5:
                enemy = enemy05;
                break;

                
            case 6:
                enemy = enemy06;
                break;

                
            case 7:
                enemy = enemy07;
                break;
        }

        getHeader.innerHTML = `<p>mrejichka</p>`
        getActions.innerHTML = `<a href="#" class="btn-prefight" onclick="PlayerMoves.calcAttack()">Attack!</a>`
        getEnemy.innerHTML = `<img src="enemies/${enemy.enemyType.toLowerCase()}.jpg" class="img-avatar">
        <div>
          <h3>${enemy.enemyType}</h3>
        </div>
        <p class="health-enemy">Health: ${enemy.health}</p>
        <p>Mana: ${enemy.mana}</p>
        <p>Strength: ${enemy.strength}</p>
        <p>Agility: ${enemy.agility}</p>
        <p>Speed: ${enemy.speed}</p>`
    }
}