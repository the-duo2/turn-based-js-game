let player;

// function Player(classType, health, mana, strength, agility , speed) {
//     this.classType = classType;
//     this.health = health;
//     this.mana = mana;
//     this.strength = strength;
//     this.agility = agility;
//     this.speed = speed;
// };

class Player {
    #classType;
    #health;
    #mana;
    #strength;
    #agility;
    #speed;
    constructor(classType, health, mana, strength, agility , speed) {
        this.classType = classType;
        this.health = health;
        this.mana = mana;
        this.strength = strength;
        this.agility = agility;
        this.speed = speed;
    }

    set classType(value) {
        this.#classType = value;
    }
    get classType() {
        return this.#classType;
    }

    set health(value) {
        this.#health = value;
    }
    get health() {
        return this.#health;
    }

    set mana(value) {
        this.#mana = value;
    }
    get mana() {
        return this.#mana;
    }

    set strength(value) {
        this.#strength = value;
    }
    get strength() {
        return this.#strength;
    }

    set agility(value) {
        this.#agility = value;
    }
    get agility() {
        return this.#agility;
    }

    set speed(value) {
        this.#speed = value;
    }
    get speed() {
        return this.#speed;
    }

}

// let PlayerMoves = {
//     calcAttack: function () {
//         // Who will attack first ? 
//         let getPlayerSpeed = player.speed;
//         let getEnemySpeed = enemy.speed;

//     }

//         //Player attacks
//     let playerAttack = function() {
//         let calcBaseDamage;
//         if (player.mana > 0) {

//             calcBaseDamage = player.strength*player.mana / 1000

//         } else {

//             calcBaseDamage = player.strength*player.agility / 1000

//         }
//         let offsetDamage = Math.floor(Math.random() * Math.floor(10));
//         let calcOutputDamage = calcBaseDamage + offsetDamage ;
//         let numberOfHits = Math.floor(Math.random() * Math.floor(player.agility/10) /2) + 1;
//         let attackValues = [calcOutputDamage, numberOfHits];
//         return attackValues;

//     }

//     let enemyAttack = function() {
//         let calcBaseDamage;
//         if (enemy.mana > 0) {

//             calcBaseDamage = enemy.strength*enemy.mana / 1000

//         } else {

//             calcBaseDamage = enemy.strength*enemy.agility / 1000

//         }
//         let offsetDamage = Math.floor(Math.random() * Math.floor(10));
//         let calcOutputDamage = calcBaseDamage + offsetDamage;
//         let numberOfHits = Math.floor(Math.random() * Math.floor(enemy.agility/10) /2) + 1;
//         let attackValues = [calcOutputDamage, numberOfHits];
//         return attackValues;

//     }

//     // Get healths for player and enemy
//     let getPlayerHealth = document.querySelector(".health-player");
//     let getEnemyHealth = document.querySelector(".health-enemy");
//     if (getPlayerSpeed > getEnemySpeed) {
//         let playerAttackValues = playerAttack();
//         let totalDamage = playerAttackValues[0] *
//     }

// }