// function Enemy(enemyType, health, mana, strength, agility , speed) {
//     this.enemyType = enemyType
//     this.health = health
//     this.mana = mana
//     this.strength = strength
//     this.agility = agility
//     this.speed = speed
// }

class Enemy {
    #enemyType;
    #health;
    #mana;
    #strength;
    #agility;
    #speed;
    constructor(enemyType, health, mana, strength, agility , speed) {
        this.enemyType = enemyType;
        this.health = health;
        this.mana = mana;
        this.strength = strength;
        this.agility = agility;
        this.speed = speed;
    }

    set enemyType(value) {
        this.#enemyType = value;
    }
    get enemyType() {
        return this.#enemyType;
    }

    set health(value) {
        this.#health = value;
    }
    get health() {
        return this.#health;
    }

    set mana(value) {
        this.#mana = value;
    }
    get mana() {
        return this.#mana;
    }

    set strength(value) {
        this.#strength = value;
    }
    get strength() {
        return this.#strength;
    }

    set agility(value) {
        this.#agility = value;
    }
    get agility() {
        return this.#agility;
    }

    set speed(value) {
        this.#speed = value;
    }
    get speed() {
        return this.#speed;
    }
}