# MnM Library

## Description

MnM Library is a JS Library for books that users can borrow, rate and leave a review.

## Project information and requirements

- Language and version: **JavaScript ES2020**
- Platform and version: **Node 14.0+**
- Core Packages: **Express**, **ESLint**

## Goals

The goal is to develop a book application that users can enjoy while using.

# Log in to the Admin user
In order to use admin rights you can use these credentials to log in or create a new user
who is going to have Basic role (you can change it through the DataBase to become Admin)

- `username`: Admin
- `password`: h2af3gse

## CRUD and Endpoints

The following CRUD operations and endpoints are implemented:

### - **GET ALL BOOKS** `GET: /books` - returns all non-deleted books, supports search by query parameters (`author`, `name`, `year` and/or `genre`) and also pagination (`page`, `pageSize` as query parameters (they are default page = 0 and pageSize = 5))
### - **GET BOOK BY ID** `GET: /books/:id` - returns a single, non-deleted book by its id 
### - **GET BOOKS BY USER ID** `GET: /books/books/:userId` - returns all books borrowed by user
### - **GET ALL REVIEWS BY BOOK ID** `GET: /books/:id/reviews` - returns all non-deleted reviews for the specified book by id
### - **GET ALL REVIEWS** `GET: /reviews` - returns all non-deleted reviews, supports limit and offset by query parameters (`page` and `pageSize`)
### - **GET REVIEW BY ID** `GET: /reviews/:id` - returns a single, non-deleted review by its id
### - **GET ALL RATINGS FOR BOOK** `GET: /books/:id/rate` - returns all ratings for a certain book
### - **USER REGISTER** `POST: /users/register` - registers a user if the body is valid, if not returns a bad request
  - body: `{ username: String, password: String, email: String, isBanned: Boolean, role: Boolean (0 - Basic, 1 - Admin) }`
### - **USER LOGIN** `POST: /users/login` - logs in a user if the body is valid, if not returns a bad request
  - body: `{ username: String, password: String }`
### - **CREATE BOOK** `POST: /books` - creates a new book if the body is valid, or returns bad request if it's not (`Admin` only)
  - body: `{ name: String, author: String, year: Number, genre: String, isBorrowed: Boolean }`
### - **BORROW BOOK** `POST: /books/:id` - borrows a book and changes its state to borrowed
### - **CREATE REVIEW** `POST: /books/:bookId/reviews` - creates a new review if the body is valid, or returns bad request if it's not
  - body: `{ content: String, isDeleted: Boolean }`
### - **UNBAN USER** `PUT: /users/:id` - unbans a user
### - **UPDATE REVIEW** `PUT: /books/:bookId/reviews/:reviewId` - updates the review content of the specified review if the body is valid, or returns bad request if it's not
  - body: `{ content: String }`
### - **CREATE REVIEW LIKE** `PUT: /books/:bookId/reviews/:reviewId/votes` - creates a new review like if the body is valid, or returns bad request if it's not
  - body: `{ like: Enum ('like', 'love', 'sad', 'heart', 'wow'), isDeleted: Boolean }`
### - **UNDELETE BOOK** `PUT: /books/books/:id` - Undeletes a book that has been previously deleted (`Admin` only)
### - **CREATE BOOK RATING** `PUT: /books/:id/rate` - should create a new book rate if the body is valid, or return bad request if it's not
  - body: `{ rating: Enum ('1', '2', '3', '4', '5'), isDeleted: Boolean }`
### - **UNLIKE REVIEW** `DELETE: /books/:bookId/reviews/:reviewId/votes` - deletes a single, existing review like by its id
### - **DELETE RATING** `DELETE: /books/:id/rate` - deletes a single, existing rating by its id (`Admin` only)
### - **DELETE REVIEW** `/books/:id/reviews/:reviewId` - deletes a single, existing review by its id
### - **RETURN BOOK** `DELETE: /books/:id` - returns a single, existing book by its id
### - **DELETE BOOK** `DELETE: /books/books/:id` - deletes a single, existing book by its id (`Admin` only)
### - **BAN USER** `DELETE: /users/:id` - bans a user by his/hers id (`Admin` only)

<br>

# Installation

### 1. Open the <span style="color: yellow"> lucky library project </span> folder in Visual Studio Code or any other tool.
   <br> ![](images_start/openFolder.png)

### 2. Open the <span style="color: yellow"> lucky library project </span> in Integrated Terminal
   <br> ![](images_start/openTerminal.png)

### 3. Run <span style="color: yellow"> npm i </span> in the Terminal to install the node modules needed to run the project.
   <br> ![](images_start/runNpmInstall.png)
   <br> ![](images_start/finishNpmInstall.png)

### 4. Create <span style="color: yellow"> .env </span> file inside the project folder and fill it with information like in the picture 
   <br>(Note: You can change the custom ones for your preference)
   <br> ![](images_start/createEnv.png)

### 5. Download <span style="color: yellow"> MariaDB </span>
   <br> ![](images_start/downloadMariaDB.png)
   <br> ![](images_start/downloadMariaDB-part2.png)

### 6. Download <span style="color: yellow"> MySQLWorkBench </span>
   <br> ![](images_start/downloadMySQLWorkbench-part1.png)
   <br> ![](images_start/downloadMySQLWorkbench-part2.png)
   <br> ![](images_start/downloadMySQLWorkbench-part3.png)
   <br> ![](images_start/downloadMySQLWorkbench-part4.png)

### 7. Now open MySQLWorkbench to set up the data base

   <br> ![](images_start/downloadMySQLWorkbench-part5.png)
   <br> ![](images_start/createSchema-part1.png)

### 8. Open <span style="color: yellow"> library_mnm.sql </span> and copy the whole file code, place it in the Workbench and execute it ⚡.

   <br> ![](images_start/createSchema-part2.png)
   <br> ![](images_start/createSchema-part3.png)

### 9. Open <span style="color: yellow"> sql-dump.sql </span>  and copy the whole file code, place it in the Workbench and execute it ⚡ to fill the database with initial data 

   <br> ![](images_start/createSchema-part4.png)

### 10. Right click 🖱️ on the <span style="color: yellow"> books </span> table to see the initial data for books 📚

   <br> ![](images_start/filledSchema.png)
   <br> ![](images_start/filledBooksTable.png)

### 11. Now download <span style="color: yellow"> Postman </span>

   <br> ![](images_start/downloadPostman-part1.png)
   <br> ![](images_start/downloadPostman-part2.png)

### 12. Open <span style="color: yellow"> Postman </span>

   <br> ![](images_start/setUpPostman-part1.png)
   <br> ![](images_start/setUpPostman-part2.png)
   <br> ![](images_start/setUpPostman-part3.png)

### 13. Start the server by <span style="color: yellow"> npm start </span> in the Integrated Terminal

   <br> ![](images_start/startServer.png)

### Note: <span style="color: red"> If everything is ok it should look like this! </span>
   <br> ![](images_start/workingServer.png)

### 14. Now open <span style="color: yellow"> Postman </span>  and start sending requests with the valid endpoints 🎉🎉🎉

   <br> ![](images_start/validRequest.png)

# Project resources and data format

The API exposes the following resources and sub-resources

## 1. Users

1. `id` - auto-increasing user identifying number (number)
2. `username` - users display name (string with length in the range [1 - 45])
3. `password` - combination of numbers and symbols (string with length in the range [1 - 255])
4. `email` - email of the user (string with length in the range [1 - 45])
5. `isBanned` - marks if user is banned by admin (boolean with default NULL)
6. `roles` - marks the role of the user (boolean with default 0)

## 2. Books

1. `id` - auto-increasing book identifying number (number)
2. `name` - title of the book (string with length in the range [1 - 100])
3. `author` - author of the book (string with length in the range [1 - 100])
4. `year` - year of writing of the book (number < 2022)
5. `genre` - genre of the book (string with length in the range [1 - 45])
6. `isBorrowed` - marks if the book is borrowed (boolean with default 0)
7. `users_id` - marks who has borrowed the book (number with default NULL)
8. `isDeleted` - marks if the book can be displayed and borrowed (boolean with default 0)

## 3. Reviews

1. `id` - auto-increasing review identifying number (number)
2. `content` - the text of the review (string with length in the range [1 - 255])
3. `isDeleted` - marks if the review is deleted by admin (boolean with default 0)
4. `books_id` - marks which book's review is this one (number)
5. `users_id` - marks who has created the review (number)

## 4. Likes

1. `id` - auto-increasing like identifying number (number) 
2. `like` - type of reaction (string in one of the ENUM('LIKE', 'LOVE', 'SAD', 'WOW', 'ANGRY'))
3. `isDeleted` - marks if the like is deleted by admin (boolean with default 0)
4. `reviews_id` - marks which review's reaction is this one (number)
5. `users_id` - marks who has reacted to the review (number)

## 5. Ratings

1. `id` - auto-increasing rating identifying number (number) 
2. `rating` - shows the rating (number in range [1 - 5])
3. `isDeleted` - marks if the rating is deleted by admin (boolean with default 0)
4. `books_id` - marks which book's rating is this one (number)
5. `users_id` - marks who has rated the book (number)

## 6. ReadBooksByUsers

1. `books_id` - marks which book was read (number)
2. `users_id` - marks who read the book (number)
3. `readOn` - marks when was the book read (DATETIME with default CURRENT_TIMESTAMP())

# Credits

### - Telerik Academy
<br>

# Authors

### - ©️Martin Yordanov
### - ©Martin Yanev