import db from './pool.js';

// Get all reviews for a specific book

const getByBookId = async (bookId) => {

    const result = await db.query(`
    SELECT * 
    FROM reviews r
    WHERE r.books_id = ? and r.isDeleted = 0
    ORDER BY r.id
    DESC
    `, [bookId]);

    // Check if reviews are found

    if (result.length === 0) {
        throw new Error('No reviews found for this book!');
    }

    return result;
    
};

// Get all reviews

const getAll = async (request) => {

    const limit = +request.query.limit || 5;
    const offset = +request.query.offset || 0;

    const searchAll =`
    SELECT * from reviews r
    WHERE r.isDeleted = 0
    LIMIT ? OFFSET ?
    `;

    const result = await db.query(searchAll, [limit, offset]);

    // Check if the reviews are found

    if (result.length === 0) {
        throw new Error('No reviews found!');
    }

    return result;
};

// Get review by id

const getById = async (id) => {
    const result = await db.query(`
    SELECT *
    FROM reviews r
    WHERE r.id = ?
    `, [id]);

    // Check if the review is found 

    if (result.length === 0) {
        throw new Error('Review with such ID was not found!');
    }
    
    return result[0];
};

// Create review

const create = async (request) => {

    // All sql queries here

    const insertReviewSql = `
    INSERT INTO reviews(content, books_id, users_id)
    VALUES (?, ?, ?)
    `;

    const reviewSelectorSql = `
    SELECT r.id, r.content, r.isDeleted, r.books_id, r.users_id
    FROM reviews r
    WHERE r.id = ?
    `;

    const adminCheck = `
    SELECT *
    FROM users u
    WHERE u.id = ? and u.role = 1`;
    
    const existenceCheckerSql = `
    SELECT *
    FROM reviews r 
    WHERE r.users_id = ? and r.books_id = ?`;

    const hasUserRedBookChecker = `
    SELECT *
    FROM readbooksbyusers r
    WHERE r.users_id = ? and r.books_id = ?`;

    const existenceCheck = (await db.query(existenceCheckerSql, [+request.user.id, +request.params.id]))[0];
    const redBookCheck = await db.query(hasUserRedBookChecker, [+request.user.id, +request.params.id]);
    const admin = await db.query(adminCheck, [+request.user.id]);

    // Checks if user is admin and allows the creation if he/she is

    // if (admin[0]) {
    //     const result = await db.query(insertReviewSql, [request.body.content, +request.params.id, +request.user.id]);
    //     return { message : `Admin created review with ID: ${result.insertId}`};
    // }

    // Checks if user has red the book in order to create a review

    if (redBookCheck.length === 0) {
        throw new Error('You cant review a book you have not read!');
    }

    // Checks if this user has already made a review about this book

    // if (existenceCheck) {
    //     throw new Error('Review by this user for this book already exists!');
    // }

    // Creates the review
    const result = await db.query(insertReviewSql, [request.body.content, +request.params.id, +request.user.id]);
    // Selects the created review
    const createdReview = (await db.query(reviewSelectorSql, [result.insertId]))[0];
    

    return {
        success: true,
        response: createdReview,
    };

};

// Update review

const update = async (request) => {

    // All sql queries here

    const checker = await db.query(`
    SELECT *
    FROM reviews
    WHERE id = ?
    `, [+request.params.reviewId]);

    const updateQuery = `
    UPDATE reviews
    SET content = ?
    WHERE id = ?
    `;

    const adminCheck = `
    SELECT *
    FROM users u
    WHERE u.id = ? and u.role = 1`;

    const admin = await db.query(adminCheck, [+request.user.id]);

    // Checks if the user is the admin, and if he/she is, allows the update

    if (admin[0]) {
        await db.query(updateQuery, [request.body.content, +request.params.reviewId]);

        return { message : `Admin updated review with ID: ${+request.params.reviewId} with content ${request.body.content}`};
    }

    // Checks if such review exists 

    if (checker.length === 0) {
        throw new Error('Review with such ID was not found!');
    }

    // Checks if the content is the same as the current one

    if (checker[0].content === request.body.content) {
        throw new Error('Content is the same!');
    }

    // Checks if the user is updating his own review

    if (checker[0].users_id !== request.user.id) {
        throw new Error('You cant update other peoples reviews!');
    }

    // Updates the review content 

    return await db.query(updateQuery, [request.body.content, +request.params.reviewId]);

};

// Delete review

const deleteReview = async (request) => {

    // All sql queries here

    const checker = await db.query(`
    SELECT *
    FROM reviews r
    WHERE r.id = ? and r.users_id = ?
    `, [+request.params.reviewId, +request.user.id]);

    const deleteQuery = `
    UPDATE reviews
    SET isDeleted = 1
    WHERE id = ?
    `;

    const adminCheck = `
    SELECT *
    FROM users u
    WHERE u.id = ? and u.role = 1`;

    const checkIfDeleted = await db.query(`
    SELECT *
    FROM reviews r
    WHERE r.id = ? and r.isDeleted = 1
    `, [+request.params.reviewId]);

    const admin = await db.query(adminCheck, [+request.user.id]);

    // Checks if the user is the admin, and if he/she is, allows the update
    
    if (admin[0]) {
        await db.query(deleteQuery, [+request.params.reviewId]);

        return { message : 'Admin deleted review !'};
    }

    // Checks if the user is deleting his own review

    if (checker.length === 0) {
        throw new Error('Unauthorized deletion!');
    }

    // Checks if review is already deleted

    if (checkIfDeleted.length === 1) {
        throw new Error('Review is already deleted!');
    }
    
    // Deletes the review

    return await db.query(deleteQuery, [+request.params.reviewId]);
    
};

// Unlike review

const unlike = async (request) => {
    
    // All sql queries here

    const checkLike = await db.query(`
    SELECT *
    FROM likes
    WHERE reviews_id = ? and users_id = ?
    `, [+request.params.reviewId, +request.user.id]);

    const checkIfAlreadyUnliked = await db.query(`
    SELECT *
    FROM likes
    WHERE reviews_id = ? and users_id = ?
    `,[+request.params.reviewId, +request.user.id]);
    
    // Check if like for this review exists

    if (checkLike.length === 0) {
        throw new Error('No like was found!');
    }

    // Checks if user has already unliked this review

    if (checkIfAlreadyUnliked[0].isDeleted === 1) {
        throw new Error('Already unliked!');
    }

    // Deletes the like
    
    return await db.query(`
    UPDATE likes
    SET isDeleted = 1
    WHERE reviews_id = ? and users_id = ?
    `,[+request.params.reviewId, +request.user.id]);

};

// Like a review

const like = async (request) => {
    
    const { bookId, reviewId } = request.params;
    const { typelike } = request.body;

    // All sql queries here

    const hasUserRedBook = `
    SELECT *
    FROM readbooksbyusers 
    WHERE books_id = ? and users_id = ?
    `;

    const hasUserLiked = `
    SELECT *
    FROM likes
    WHERE users_id = ? and reviews_id = ?
    `;

    const createLike = `
    INSERT INTO likes (typelike, reviews_id, users_id)
    VALUES (?, ?, ?)
    `;

    const validateUserRed = (await db.query(hasUserRedBook, [+bookId, +request.user.id]));
    const checker = (await db.query(hasUserLiked, [+request.user.id, +reviewId]))[0];

    // Checks if user has red the book in order to like the review

    if (validateUserRed.length === 0) {
        throw new Error('User has not red this book!');
    }

    // Check if the user has already liked the review

    if (checker) {
        throw new Error ('You cant like this review twice!');
    }

    // Checks for the enums in reaction

    if (typelike !== 'SAD' 
    && typelike !== 'WOW' 
    && typelike !== 'LIKE'
    && typelike !== 'LOVE'
    && typelike !== 'HEART') {
        throw new Error ('Like should be string - SAD, WOW, LIKE, LOVE or HEART');
    }

    // Creates the like in the table

    return await db.query(createLike, [typelike, +reviewId, +request.user.id]);

};

export default {
    getByBookId,
    getAll,
    getById,
    create,
    like,
    unlike,
    deleteReview,
    update,

};
