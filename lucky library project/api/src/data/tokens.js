import db from './pool.js';

export const tokenExists = async (token) => {

    // Selects the token and checks if it exists

    const results = await db.query(`
    SELECT * 
    FROM tokens t
    WHERE t.token = ?
    `, [token]);

    return results && results.length > 0;
    
};

