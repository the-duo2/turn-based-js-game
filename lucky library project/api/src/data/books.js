import db from './pool.js';

const getAll = async (page, pageSize) => {
    const sql = `
        SELECT * 
        FROM books b
        WHERE b.isDeleted = '0'
        LIMIT ? OFFSET ?
    `;
    
    return await db.query(sql, [ pageSize, (page * pageSize)]);
};

const getAllByQuery = async (arr) => {
    const sql = `
    SELECT * 
    FROM books 
    WHERE 
    `;
    const sql2 = arr
        .map(([key, value]) => `${key} LIKE '%${value}%'`)
        .join(' AND ');
    const sql3= `LIMIT 16`;
    return await db.query((sql+sql2+sql3));
};

const existingBook = async (name) => {
    const sql = `
    SELECT *
    FROM books
    WHERE name = ?
    `;

    return await db.query(sql, [name]);
};

const getById = async (id) => {  // ready
    const sql = `
    SELECT * 
    FROM books b 
    WHERE b.id = ?
    `;

    const book = await db.query(sql,[id]);
    
    return book[0];
};

const getByUserId = async (userId) => {
    const sql = `
    SELECT *
    FROM books b
    WHERE b.users_id = ?
    `;

    return await db.query(sql,[userId]);
};

const createBook = async (name, author, year, genre, description, coverUrl) => { // ready
    const sql = `
        INSERT INTO books(name, author, year, genre, description, coverUrl)
        VALUES (?, ?, ?, ?, ?, ?)
    `;

    const result = await db.query(sql, [name, author, year, genre, description, coverUrl]);

    return {
        id: result.insertId,
        name: name,
        author: author,
        year: year,
        genre: genre,
        description: description,
        coverUrl:coverUrl
    };
};

const deleteABook = async (id) => {
    const sql = `
    UPDATE books
    SET isDeleted = 1
    WHERE id = ? 
    `;

    await db.query(sql, [id]);
};

const undeleteABook = async (id) => {
    const sql = `
    UPDATE books
    SET isDeleted = 0
    WHERE id = ? 
    `;

    await db.query(sql, [id]);
};


const borrowById = async (id, userId) => {
    const sql = `
        UPDATE books
        SET isBorrowed = 1, users_id = ${userId}
        WHERE id = ${id} 
    `;

    await db.query(sql);

    const sql2 = `
        REPLACE INTO readbooksbyusers(books_id, users_id)
        VALUES(${id},${userId})
    `;
    await db.query(sql2);
};

const returnById = async (id) => {
    const sql = `
        UPDATE books
        SET isBorrowed = 0 , users_id = NULL
        WHERE id = ${id} 
    `;

    await db.query(sql);
};

const getAllRateByBookId = async (bookId) => {
    const sql = `
        SELECT *
        FROM ratings
        WHERE books_id = ?
    `;

    return await db.query(sql, [bookId]);
};

const createRate = async (bookId, userId, rate) => {
    const sql = `
    INSERT INTO ratings(rating, books_id, users_id)
    VALUES (?, ?, ?)
    `;

    await db.query(sql, [rate, bookId, userId]);
};

const updateRate = async (bookId, userId, rate) => {
    const sql = `
    UPDATE ratings
    SET rating = ?
    WHERE books_id = ? AND users_id = ?
    `;

    await db.query(sql,[rate, bookId, userId]);
};

const deleteRate = async (bookId, userId) => {
    const sql = `
    UPDATE ratings
    SET isDeleted = 1
    WHERE books_id = ? AND users_id = ?
    `;

    await db.query(sql, [bookId, userId]);
};

const checkRead = async (bookId, userId) => {
    const sql = `
        SELECT *
        FROM readbooksbyusers 
        WHERE users_id = ${userId} and books_id = ?
    `;

    const book = await db.query(sql, [bookId]);

    if ( book.length === 0) {
        return false;
    }

    return true;
};

const checkRate = async (bookId, userId) => {
    const sql = `
        SELECT *
        FROM ratings
        WHERE books_id = ? and users_id = ?
    `;

    const checker = (await db.query(sql, [bookId, userId]))[0];

    if (checker) {
        return true;
    }

    return false;
};

const checkDeletedRate = async (bookId, userId) => {
    const sql = `
        SELECT *
        FROM ratings
        WHERE books_id = ? and users_id = ? 
    `;

    return await db.query(sql, [bookId, userId]);
};


export default {
    getAll,
    getAllByQuery,
    getById,
    getByUserId,
    createBook,
    deleteABook,
    undeleteABook,
    borrowById,
    returnById,
    createRate,
    checkRead,
    getAllRateByBookId,
    checkRate,
    updateRate,
    deleteRate,
    checkDeletedRate,
    existingBook,
};