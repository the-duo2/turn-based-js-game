import express from 'express';
import validateBody from '../middlewares/validate-body.js';
import validateBook from '../validators/validate-book.js';
import validateReview from '../validators/validate-review.js';
import validateParams from '../middlewares/validate-params.js';
import { authMiddleware } from '../auth/auth-middleware.js';
import booksService from '../services/books-service.js';
import booksData from '../data/books.js';
import reviewsService from '../services/reviews-service.js';
import reviewsData from '../data/reviews.js';
import bookMessageService from '../services/book-message-service.js';
import validateRating from '../validators/validate-rating.js';
import validateBodyKeys from '../middlewares/validate-body-keys.js';
import validateQueryKeys from '../middlewares/validate-query-keys.js';
import validateQueryKeysSchema from '../validators/validate-query-keys-schema.js';
import validateAdmin from '../middlewares/validate-admin.js';
import loggedGuard from '../middlewares/token-verification.js';
import multer from 'multer';
import path from 'path';

const booksController = express.Router();

const storage = multer.diskStorage({

    destination(req, file, cb) {
      cb(null, 'coverUrl');
    },
    filename(req, file, cb) {
      // for book upload / update
      // 1 - check if req.user is admin
      // 2 - if admin find the book by bookId
      // 3 - if the book has a picture, return the picture name
      // 4 - otherwise create a new picture name
      const filename = Date.now() + path.extname(file.originalname);
  
      cb(null, filename);
    },
    });
  
    const upload = multer({ storage });

booksController

    // retrieve all books

    .get('/', validateQueryKeys('getAll', validateQueryKeysSchema), async (req, res) => {
        try {
            const books = await booksService.getAllBooks(booksData)(req.query);

            if (books.length === 0) {
                return res.json({
                    msg: 'No such books on this page',
                });
            }

            res.status(200).json(books);
        } catch (error) {
            res.status(400).json ({
                error: error.message,
            });
        } 
    })

    // view single book

    .get('/:id', validateParams, async (req, res) => {
        try {
            const { id } = req.params;
            const book = await booksService.getBookById(booksData)(+id);

            res.status(200).json(book);
        } catch (error) {
            res.status(404).json ({
                error: error.message,
            });
        }
    })

    // Get books by user

    .get('/books/:userId', authMiddleware, loggedGuard, validateParams, async (req, res) => {
        try {
            const { userId } = req.params;
            const books = await booksService.getBookByUserId(booksData)(+userId);

            res.status(200).json(books);
        } catch (error) {
            res.status(404).json ({
                error: error.message,
            });
        }
    })

    
    // Creates a book

    .post('/', authMiddleware, loggedGuard,validateBody('book', validateBook), async (req, res) => {
        try {
            const book = req.body;
            const createdBook = await booksService.createBook(booksData)(book);

            res.status(200).json(createdBook);
        } catch (error) {
            res.status(409).json({
                error: error.message,
            });
        }
    })

    //Upload cover image
    
    .put('/coverUrl', authMiddleware, loggedGuard, upload.single('coverUrl'), async(req,res) => {
        try{

            res.json({message : req.file.filename});
        } catch (error) {
            res.json({message: error});
        }
    })

    // Delete a book (only admin)

    .delete('/books/:id', authMiddleware, loggedGuard, validateAdmin, validateParams, async (req, res) => {
        try {
            const { id } = req.params;
            const deletedBook = await booksService.deleteBook(booksData)(+id);

            res.status(200).json(deletedBook);
        } catch(error) {
            if (error.message === bookMessageService.NOT_FOUND) {
                return res.status(404).json({
                    error: error.message,
                });
            }
            if (error.message === bookMessageService.CAN_NOT_DELETE_TWICE) {
                return res.status(409).json({
                    error: error.message,
                });
            }
        }
    })

    // Undelete a book (only admin)

    .put('/books/:id', authMiddleware, loggedGuard, validateAdmin, validateParams,  async (req, res) => {
        try {
            const { id } = req.params;
            const undeletedBook = await booksService.undeleteBook(booksData)(+id);

            res.status(200).json(undeletedBook);
        } catch (error) {
            if (error.message === bookMessageService.NOT_FOUND) {
                return res.status(404).json({
                    error: error.message,
                });
            }
            if (error.message === bookMessageService.NOT_DELETED) {
                return res.status(409).json({
                    error: error.message,
                });
            }
        }
    })

    // Borrows a book by id

    .post('/:id', authMiddleware, loggedGuard, validateParams, async (req, res) => { // ready
        try {
            const bookId = +req.params.id;
            const userId = +req.user.id;
            const book = await booksService.borrowBookById(booksData)(bookId, userId);
    
            res.status(200).json(book);
        } catch (error) {
            if (error.message === bookMessageService.NOT_FOUND) {
                return res.status(404).json({
                    error: error.message,
                });
            }
            if (error.message === bookMessageService.BOOK_CAN_NOT_BE_BORROWED) {
                return res.status(404).json({
                    error: error.message,
                });
            }
            if (error.message === bookMessageService.ALREADY_BORROWED) {
                return res.status(409).json({
                    error: error.message,
                });
            }
        }
    })

    // return a book

    .delete('/:id', authMiddleware, loggedGuard, validateParams,  async (req ,res) => { // ready
        try {
            const bookId = +req.params.id;
            const userId = +req.user.id;
            const book = await booksService.returnBookById(booksData)(bookId, userId);

            res.status(200).json(book);
        } catch (error) {
            if (error.message === bookMessageService.NOT_FOUND) {
                return res.status(404).json({
                    error: error.message,
                });
            }
            if (error.message === bookMessageService.NOT_OWNED) {
                return res.status(409).json({
                    error: error.message,
                });
            }
        }
    })

    // Get all reviews for a book

    .get('/:id/reviews', validateParams, async (req, res) => {
        try {
            const reviews = await reviewsService.getReviewsByBookId(reviewsData)(+req.params.id);
            res.json(reviews);
        } catch (error) {
            res.status(400).json({ error : error.message});
        }
    })

    //Get a specific review by id

    .get('/:bookId/reviews/:reviewId', authMiddleware, loggedGuard, validateParams, async (req, res) => {
        try {
            res.json((await reviewsService.getReviewById(reviewsData)(+req.params.reviewId)));
        } catch (error) {
            res.json({ error : error.message });
        }
    })

    // Create review

    .post('/:id/reviews', authMiddleware, loggedGuard, validateParams, validateBody('review', validateReview), async (req, res) => {
        try {
            const review =  await reviewsService.createReview(reviewsData)(req);

            res.json(review);
        } catch (error) {
            res.status(400).json({ error : error.message });
        }
    })

    // Update review content

    .put('/:bookId/reviews/:reviewId', authMiddleware, loggedGuard, validateParams, validateBody('review', validateReview), async (req, res) => {
        try {
            await reviewsService.updateReview(reviewsData)(req);

            res.status(200).json({
                message: `Review updated with content: ${req.body.content}`,
            });
        } catch (error) {
            res.status(401).json({ error : error.message });
        }
    })

    // Delete review

    .delete('/:id/reviews/:reviewId', authMiddleware, loggedGuard, validateParams, async (req, res) => {
        try {
            await reviewsService.deleteReview(reviewsData)(req);

            res.status(200).json({
                message: 'Review deleted!',
            });
        } catch (error) {
            res.json({ error : error.message });
        }
    })

    // Get all ratings for book

    .get('/:id/rate', validateParams, async (req, res) => {
        try {
            const { id } = req.params; 
            console.log(id);
            const ratings = await booksService.getAllRatesByBook(booksData)(+id);
            console.log(ratings);
            res.status(200).json(ratings);
        } catch (error) {
            res.status(400).json ({
                error: error.message,
            });
        }
    })

    // Rate book

    .put('/:id/rate', authMiddleware, loggedGuard, validateParams, validateBodyKeys(validateRating), validateBody('rating', validateRating), async (req, res) => {
        try {
            const bookId = + req.params.id;
            const userId = + req.user.id;
            const rating = + req.body.rating;
            const response = await booksService.createRating(booksData)(bookId, userId, rating);

            res.status(200).json({response});
        } catch (error) {
            if (error.message === bookMessageService.NOT_FOUND) {
                return res.status(404).json({
                    error: error.message,
                });
            }
            if (error.message === bookMessageService.NOT_READ_BOOK) {
                return res.status(409).json({
                    error: error.message,
                });
            }
        }
    })

    // Delete rating

    .delete('/:id/rate',authMiddleware, loggedGuard, validateAdmin, validateParams, async (req, res) => {
        try {
            const bookId = +req.params.id;
            const userId = +req.user.id;

            const deleteRate = await booksService.deleteRating(booksData)(bookId, userId);

            res.status(200).json({
                message: deleteRate,
            });
        } catch (error) {
            if (error.message === bookMessageService.NOT_FOUND) {
                return res.status(404).json({
                    error: error.message,
                });
            }
            if (error.message === bookMessageService.CAN_NOT_DELETE_TWICE) {
                return res.status(409).json({
                    error: error.message,
                });
            }
        }
    })

    // Like review

    .put('/:bookId/reviews/:reviewId/votes', authMiddleware, loggedGuard, validateParams, async (req, res) => {
        try {
            await reviewsService.createLike(reviewsData)(req);
            res.send({message : 'rating created'});
        } catch (error) {
            res.send({ error : error.message });
        }
    
    })

    // Deletes a like from a user for a given review

    .delete('/:bookId/reviews/:reviewId/votes', authMiddleware, loggedGuard, validateParams, async (req, res) => {
        try {
            await reviewsService.deleteLike(reviewsData)(req);
            res.send({ message : 'Unliked!'});
        } catch (error) {
            res.status(400).json({ error : error.message });
        }
    });

export default booksController;