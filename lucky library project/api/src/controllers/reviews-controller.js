import express from 'express';
import reviewsService from '../services/reviews-service.js';
import reviewsData from '../data/reviews.js';
import validateParams from '../middlewares/validate-params.js';
import { authMiddleware } from '../auth/auth-middleware.js';
import loggedGuard from '../middlewares/token-verification.js';
 
const reviewsController = express.Router();

reviewsController


    // Get all reviews created

    .get('/', authMiddleware, loggedGuard, validateParams, async (req, res) => {
        try {
            const reviews = await reviewsService.getAllReviews(reviewsData)(req);
            res.json(reviews);
        } catch (error) {
            res.json({ error : error.message });
        }
    })

    //Get a specific review by id
    
    .get('/:id', authMiddleware, loggedGuard, validateParams, async (req, res) => {
        try {
            const review = await reviewsService.getReviewById(reviewsData)(+req.params.id);
            res.json(review);
        } catch (error) {
            res.json({ error : error.message });
        }
    });

export default reviewsController;

