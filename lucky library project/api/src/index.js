import express from 'express';
import helmet from 'helmet';
import cors from 'cors';
import dotenv from 'dotenv';
import passport from 'passport';
import jwtStrategy from './auth/strategy.js';
import booksController from './controllers/books-controller.js';
import usersController from './controllers/users-controller.js';
import reviewsController from './controllers/reviews-controller.js';

const config = dotenv.config().parsed;

const PORT = config.PORT;
const app = express();

app.use(express.json());
app.use(helmet());
app.use(cors());

passport.use(jwtStrategy);
app.use(passport.initialize());
//middle
app.use('/books', booksController);
app.use('/users', usersController);
app.use('/reviews', reviewsController);
app.use('/app', express.static('public'));
app.use('/avatars', express.static('avatars'));
app.use('/coverUrl', express.static('coverUrl'));

app.listen(PORT, () => console.log(`Listening on port ${PORT}`));