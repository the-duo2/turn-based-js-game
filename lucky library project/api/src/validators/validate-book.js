const minNameLength = 0;
const maxNameLength = 40;
const maxYear = 2022;

export default {
    name: (value) => typeof value === 'string' && value.length > minNameLength && value.length < maxNameLength,
    author: (value) => typeof value === 'string' && value.length > minNameLength && value.length < maxNameLength,
    year: (value) => typeof value === 'number' && value <maxYear,
    genre: (value) => typeof value === 'string' && value.length > 0,
};