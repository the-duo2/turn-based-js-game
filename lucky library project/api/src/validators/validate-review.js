const minContentLength = 10;
const maxContentLength = 210;

export default {
    content: (value) => typeof value === 'string' && value.length > minContentLength && value.length < maxContentLength,
};