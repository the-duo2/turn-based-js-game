const minRating = 1;
const maxRating = 5;

export default {
    rating: (value) => typeof value === 'number' && value >= minRating && value <= maxRating,
};