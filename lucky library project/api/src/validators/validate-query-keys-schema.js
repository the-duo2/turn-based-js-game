export default {
    getAll: {
        name: (value) => typeof value === 'string' || undefined,
        author: (value) => typeof value === 'string' || undefined,
        year: (value) => typeof value === 'string' || undefined,
        genre: (value) => typeof value === 'string' || undefined,
        page: (value) => typeof value === 'string'  || undefined,
        pageSize: (value) => typeof value === 'string' || undefined,
    },
};