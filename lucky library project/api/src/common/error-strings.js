export default {
    book: {
        name: 'Name must be [1 - 100] chars',
        genre: 'Genre must be string between [1-45]',
        author: 'Author must be [1 - 100] chars',
        year: 'Year must be before 2022',
    },
    review: {
        content: 'Content must be [30 - 210] chars',
    },
    user: {
        username: 'Username should be between 5 and 15 characters, and should contain letters and numbers!',
        password: 'Password should be between 5 and 15 characters, and should contain letters and numbers!',
        email: 'Email should be of valid format!',
    },
    rating: {
        rating: 'Rating must be a number between 1 and 5',
    },
};