const getAllReviews = reviewsData => {
    return async (request) => {
        return await reviewsData.getAll(request);
    };
};

const getReviewsByBookId = reviewsData => {
    return async(bookId) => {
        return await reviewsData.getByBookId(bookId);
    };
};

const getReviewById = reviewsData => {
    return async (id) => {
        return await reviewsData.getById(id);
    };
};

const createReview = reviewsData => {
    return async (request) => {
        return await reviewsData.create(request);
    };
};

const deleteReview = reviewsData => {
    return async (request) => {
        return await reviewsData.deleteReview(request);
    };
};

const updateReview = reviewsData => {
    return async (request) => {
        return await reviewsData.update(request);
    };
};

const createLike = reviewsData => {
    return async (request) => {
        return await reviewsData.like(request);
    };
};

const deleteLike = reviewsData => {
    return async (request) => {
        return await reviewsData.unlike(request);
    };
};

export default {
    getAllReviews,
    getReviewById,
    getReviewsByBookId,
    createReview,
    deleteLike,
    createLike,
    deleteReview,
    updateReview,
};