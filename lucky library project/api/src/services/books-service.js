import BookMsgService from './book-message-service.js';

const getAllBooks = booksData => {
    return async (queries) => {
        let { page = 0, pageSize = 16 } = queries;
        const arr = Object.keys(queries).map(k => [k, queries[k]]).filter(key => key[0] !== 'page' && key[0] !== 'pageSize');
        
        if (page < 0) {
            page = 0;
        }
        if (pageSize < 16) {
            pageSize = 16;
        }
        if (pageSize > 20) {
            pageSize = 20;
        }
        if (arr.length > 0) {
            return await booksData.getAllByQuery(arr);
        }
        return await booksData.getAll(+page, +pageSize);
    };
};

const getBookById = booksData => {
    return async (id) => {
        const book = await booksData.getById(id);

        if (!book) {
            throw new Error (BookMsgService.NOT_FOUND);
        }

        if (book.isDeleted === 1) {
            throw new Error (BookMsgService.NOT_FOUND);
        }

        return book;
    };
};

const getBookByUserId = booksData => {
    return async (userId) => {
        const books = await booksData.getByUserId(userId);

        return books;
    };
};

const createBook = booksData => {
    return async (book) => {
        const { name, author, year, genre, description, coverUrl } = book;

        const existingBook = await booksData.existingBook(name);
        
        if (existingBook.length === 1) {
            throw new Error (BookMsgService.DUPLICATE_BOOK);
        }

        return await booksData.createBook(name, author, year, genre, description, coverUrl);
    };
};

const deleteBook = booksData => {
    return async (id) => {
        const book = await booksData.getById(id);

        if (!book) {
            throw new Error (BookMsgService.NOT_FOUND);
        }

        if (book.isDeleted === 1) {
            throw new Error (BookMsgService.CAN_NOT_DELETE_TWICE);
        }

        await booksData.deleteABook(id);

        return await booksData.getById(id);
    };
};

const undeleteBook = booksData => {
    return async (id) => {
        const book = await booksData.getById(id);

        if (!book) {
            throw new Error (BookMsgService.NOT_FOUND);
        }

        if (book.isDeleted === 0) {
            throw new Error (BookMsgService.NOT_DELETED);
        }

        await booksData.undeleteABook(id);

        return await booksData.getById(id);
    };
};

const borrowBookById = booksData => {
    return async (id, userId) => {
        const book = await booksData.getById(id);

        if (!book) {
            throw new Error (BookMsgService.NOT_FOUND);
        }

        if (book.isDeleted !== 0) {
            throw new Error (BookMsgService.BOOK_CAN_NOT_BE_BORROWED);
        }

        if (book.isBorrowed !== 0) {
            throw new Error (BookMsgService.ALREADY_BORROWED);
        }

        await booksData.borrowById(id, userId);

        return await booksData.getById(id);
    };
};

const returnBookById = booksData => {
    return async (id, userId) => {
        const book = await booksData.getById(id);

        if (!book) {
            throw new Error (BookMsgService.NOT_FOUND);
        }
        
        if (book.isBorrowed !== 1 || book.users_id !== userId) {
            throw new Error (BookMsgService.NOT_OWNED);
        }
        
        await booksData.returnById(id);

        return await booksData.getById(id);
    };
};

const getAllRatesByBook = booksData => {
    return async (bookId) => {
        return await booksData.getAllRateByBookId(bookId);
    };
};

const createRating = booksData => {
    return async (bookId, userId, rate) => {
        const book = await booksData.getById(bookId);

        if (!book) {
            throw new Error (BookMsgService.NOT_FOUND);
        }

        const validateRead = await booksData.checkRead(bookId, userId);

        if (!validateRead) {
            throw new Error (BookMsgService.NOT_READ_BOOK);
        }

        const checkRate = await booksData.checkRate(bookId, userId);

        if (checkRate) {
            await booksData.updateRate(bookId, userId, rate);

            return BookMsgService.RATING_UPDATED;
        }

        await booksData.createRate(bookId, userId, rate);

        return BookMsgService.RATING_CREATED;
    };
};

const deleteRating = booksData => {
    return async (bookId, userId) => {
        const checkRate = (await booksData.checkDeletedRate(bookId, userId))[0];
        
        if (!checkRate) {
            throw new Error (BookMsgService.NOT_FOUND);
        }
        if (checkRate.isDeleted === 1) {
            throw new Error (BookMsgService.CAN_NOT_DELETE_TWICE);
        }
        
        await booksData.deleteRate(bookId, userId);

        return BookMsgService.RATING_DELETED;
    };
};

export default {
    getAllBooks,
    getBookById,
    getBookByUserId,
    createBook,
    deleteBook,
    undeleteBook,
    borrowBookById,
    returnBookById,
    getAllRatesByBook,
    createRating,
    deleteRating,
};