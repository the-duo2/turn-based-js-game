import jwt from 'jsonwebtoken';
import dotenv from 'dotenv';

const SECRET_KEY = dotenv.config().parsed.SECRET_KEY;

const createToken = (payload) => {
    const token = jwt.sign(
        payload,
        SECRET_KEY,
    );

    return token;
};


export default createToken;