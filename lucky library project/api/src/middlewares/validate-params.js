// Validates if the parameters are numbers or not

export default async (req, res, next) => {
    try{
        if (Object.values(req.params).some(isNaN)) {
            throw new Error('Parameters must be numbers!');
        }
        await next();
    } catch (error) {
        res.status(400).json({ error : error.message });
    } 
};
