import db from '../data/pool.js';

export default async (req, res, next) => {
    const sql = `
    SELECT *
    FROM users u
    WHERE u.id = ?`;

    const admin = (await db.query(sql, [req.user.id]))[0];
    if (admin.role === 0) {
        return res.status(401).json({
            msg: 'UNAUTHORIZED',
        });
    }

    await next();
};