export default (resource, validator) => async (req, res, next) => {
    Object
        .keys(req.query)
        .forEach(key => {
            if (!validator[resource][key]) {
                delete req.query[key];
            }
        });
    await next();
};