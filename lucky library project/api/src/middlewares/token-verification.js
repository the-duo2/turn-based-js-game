import { tokenExists } from '../data/tokens.js';

export default async (req, res, next) => {

    // Selects the token

    const token = req.headers.authorization.replace('Bearer', '');
    
    // If the token doesnt exist returns an error 
    
    if (await tokenExists(token)) {
        res.status(401).json({ error : 'You are not logged in !'});
    }

    next();
};


