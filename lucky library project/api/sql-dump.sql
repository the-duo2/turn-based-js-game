INSERT INTO library_mnm.books (name, author, year, genre, description, coverUrl)
VALUES  
('In Search of Lost Time', 'Marcel Proust', 1913, 'Modernism', 'In Search of Lost Time, also translated 
as Remembrance of Things Past, is a novel in seven volumes by Marcel Proust (1871–1922).
It is his most prominent work, known both for its length and its theme of involuntary memory; 
the most famous example of this is the "episode of the madeleine", which occurs early in the first volume.
It gained fame in English in translations by C. K. Scott Moncrieff and Terence Kilmartin as Remembrance 
of Things Past, but the title In Search of Lost Time, a literal rendering of the French, became ascendant 
after D. J. Enright adopted it for his revised translation published in 1992.', 'InSearchOfLostTime.jpg'),
('Ulysses', 'James Joyce', 1922, 'Novel', 'Ulysses is a modernist novel by Irish writer James Joyce. It was
first serialized in parts in the American journal The Little Review from March 1918 to December 1920 and then
published in its entirety in Paris by Sylvia Beach on 2 February 1922, Joyce\'s 40th birthday. It is considered
one of the most important works of modernist literature and has been called "a demonstration and summation of 
the entire movement." According to Declan Kiberd, "Before Joyce, no writer of fiction had so foregrounded the 
process of thinking". Ulysses chronicles the appointments and encounters of the itinerant Leopold Bloom in Dublin
in the course of an ordinary day, 16 June 1904. Ulysses is the Latinised name of Odysseus, the hero of Homer\'s
epic poem the Odyssey, and the novel establishes a series of parallels between the poem and the novel, with 
structural correspondences between the characters and experiences of Bloom and Odysseus, Molly Bloom and Penelope,
and Stephen Dedalus and Telemachus, in addition to events and themes of the early 20th-century context of modernism,
Dublin, and Ireland\'s relationship to Britain. The novel is highly allusive and also imitates the styles of different
periods of English literature. Since its publication, the book has attracted controversy and scrutiny, ranging from an
obscenity trial in the United States in 1921 to protracted textual "Joyce Wars". The novel\'s stream of consciousness
technique, careful structuring, and experimental prose—replete with puns, parodies, and allusions—as well as its rich
characterisation and broad humour have led it to be regarded as one of the greatest literary works in history; Joyce
fans worldwide now celebrate 16 June as Bloomsday.', 'Ulysses.jpg'),
('Don Quixote', 'Miguel de Cervantes', 1615, 'Novel', 'The Ingenious Gentleman Don Quixote of La Mancha don Quijote de 
la Mancha is a Spanish novel by Miguel de Cervantes. It was originally published in two parts, in 1605 and 1615. A 
founding work of Western literature, it is often labeled "the first modern European novel" and is considered one of the
greatest novels ever written. Don Quixote also holds the distinction of being the eleventh most-translated book in the 
world, the first being the Bible. The plot revolves around the adventures of a noble from La Mancha named Alonso Quixano,
who reads so many chivalric romances that he loses his mind and decides to become a knight-errant  to revive chivalry and
serve his nation, under the name Don Quixote de la Mancha. He recruits a simple farmer, Sancho Panza, as his squire, who 
often employs a unique, earthy wit in dealing with Don Quixote\'s rhetorical monologues on knighthood, already considered
old-fashioned at the time. Don Quixote, in the first part of the book, does not see the world for what it is and prefers 
to imagine that he is living out a knightly story. The book had a major influence on the literary community, as evidenced 
by direct references in Alexandre Dumas\' The Three Musketeers (1844), Mark Twain\'s Adventures of Huckleberry Finn (1884),
and Edmond Rostand\'s Cyrano de Bergerac (1897), as well as the word quixotic and the epithet Lothario; the latter refers
to a character in "El curioso impertinente" ("The Impertinently Curious Man"), an intercalated story that appears in Part 
One, chapters 33–35. The 19th-century German philosopher Arthur Schopenhauer cited Don Quixote as one of the four greatest
novels ever written.','DonQuixote.jpg'),
('One Hundred Years of Solitude', 'Gabriel Garcia Marquez', 1970, 'Magic realism', 'One Hundred Years of is a landmark 1967
novel by Colombian author Gabriel García Márquez that tells the multi-generational story of the Buendía family, whose 
patriarch, José Arcadio Buendía, founded the (fictitious) town of Macondo. The novel is often cited as one of the supreme 
achievements in literature. The magical realist style and thematic substance of One Hundred Years of Solitude established 
it as an important representative novel of the literary Latin American Boom of the 1960s and 1970s, which was stylistically 
influenced by Modernism (European and North American) and the Cuban Vanguardia (Avant-Garde) literary movement. Since it was 
first published in May 1967 in Buenos Aires by Editorial Sudamericana, One Hundred Years of Solitude has been translated into 
46 languages and sold more than 50 million copies. The novel, considered García Márquez\'s magnum opus, remains widely 
acclaimed and is recognized as one of the most significant works both in the Hispanic literary canon and in world literature.',
'OneHundredYearsOfSolitude.jpg'),
('The Great Gatsby', 'F. Scott Fitzgerald', 1925, 'Tragedy', 'The Great Gatsby is a 1925 novel by American writer F. Scott 
Fitzgerald. Set in the Jazz Age on Long Island, the novel depicts narrator Nick Carraway\'s interactions with mysterious 
millionaire Jay Gatsby and Gatsby\'s obsession to reunite with his former lover, Daisy Buchanan. A youthful romance Fitzgerald
had with socialite Ginevra King, and the riotous parties he attended on Long Island\'s North Shore in 1922 inspired the novel.
Following a move to the French Riviera, he completed a rough draft in 1924. He submitted the draft to editor Maxwell Perkins,
who persuaded Fitzgerald to revise the work over the following winter. After his revisions, Fitzgerald was satisfied with the 
text, but remained ambivalent about the booK\'s title and considered several alternatives. The final title he desired was Under
the Red, White, and Blue. Painter Francis Cugat\'s final cover design impressed Fitzgerald who incorporated a visual element 
from the art into the novel. After its publication by Scribner\'s in April 1925, The Great Gatsby received generally favorable 
reviews, although some literary critics believed it did not hold up to Fitzgerald\'s previous efforts and signaled the end of 
the author\'s literary achievements. Despite the warm critical reception, Gatsby was a commercial failure. The book sold fewer 
than 20,000 copies by October, and Fitzgerald\'s hopes of a monetary windfall from the novel were unrealized. When the author 
died in 1940, he believed himself to be a failure and his work forgotten. After his death, the novel faced a critical and 
scholarly re-examination amid World War II, and it soon became a core part of most American high school curricula and a focus 
of American popular culture. Numerous stage and film adaptations followed in the subsequent decades.','TheGreatGatsby.jpg'),
('Moby Dick', 'Herman Melville', 1851, 'Novel', 'Moby-Dick; or, The Whale is an 1851 novel by American writer Herman Melville. 
The book is the sailor Ishmael\'s narrative of the obsessive quest of Ahab, captain of the whaling ship Pequod, for revenge on 
Moby Dick, the giant white sperm whale that on the ship\'s previous voyage bit off Ahab\'s leg at the knee. A contribution to 
the literature of the American Renaissance, Moby-Dick was published to mixed reviews, was a commercial failure, and was out of 
print at the time of the author\'s death in 1891. Its reputation as a "Great American Novel" was established only in the 20th 
century, after the centennial of its author\'s birth. William Faulkner said he wished he had written the book himself, and D. H. 
Lawrence called it "one of the strangest and most wonderful books in the world" and "the greatest book of the sea ever written". 
Its opening sentence, "Call me Ishmael", is among world literature\'s most famous. Melville began writing Moby-Dick in February 
1850, and finished 18 months later, a year longer than he had anticipated. Melville drew on his experience as a common sailor 
from 1841 to 1844, including several years on whalers, and on wide reading in whaling literature. The white whale is modeled on 
the notoriously hard-to-catch albino whale Mocha Dick, and the book\'s ending is based on the sinking of the whaleship Essex in 
1820. His literary influences include Shakespeare and the Bible. The detailed and realistic descriptions of whale hunting and of 
extracting whale oil, as well as life aboard ship among a culturally diverse crew, are mixed with exploration of class and social 
status, good and evil, and the existence of God. In addition to narrative prose, Melville uses styles and literary devices 
ranging from songs, poetry, and catalogs to Shakespearean stage directions, soliloquies, and asides. In August 1850, with the 
manuscript perhaps half finished, he met Nathaniel Hawthorne and was deeply moved by his Mosses from an Old Manse, which he 
compared to Shakespeare in its cosmic ambitions. This encounter may have inspired him to revise and expand Moby-Dick, which is 
dedicated to Hawthorne, "in token of my admiration for his genius".', 'MobyDick.jpg'),
('War and Peace', 'Leo Tolstoy', 1869, 'Novel', 'War and Peace is a novel by the Russian author Leo Tolstoy, first published 
serially, then published in its entirety in 1869. It is regarded as one of Tolstoy\'s finest literary achievements and remains 
an internationally praised classic of world literature. The novel chronicles the French invasion of Russia and the impact of 
the Napoleonic era on Tsarist society through the stories of five Russian aristocratic families. Portions of an earlier version, 
titled The Year 1805, were serialized in The Russian Messenger from 1865 to 1867 before the novel was published in its entirety 
in 1869. Tolstoy said War and Peace is "not a novel, even less is it a poem, and still less a historical chronicle." Large 
sections, especially the later chapters, are philosophical discussions rather than narrative. Tolstoy also said that the best 
Russian literature does not conform to standards and hence hesitated to call War and Peace a novel. Instead, he regarded Anna 
Karenina as his first true novel. The term "war and peace" is sometimes used in general language to indicate something that is 
very long or comprehensive; for example, "I just need a short briefing, it doesn\'t have to be war and peace".', 'WarAndPeace.jpg'),
('Hamlet', 'William Shakespeare', 1870, 'Tragedy', 'The Tragedy of Hamlet, Prince of Denmark, often shortened to Hamlet, is a 
tragedy written by William Shakespeare sometime between 1599 and 1601. It is Shakespeare\'s longest play, with 29,551 words. 
Set in Denmark, the play depicts Prince Hamlet and his revenge against his uncle, Claudius, who has murdered Hamlet\'s father 
in order to seize his throne and marry Hamlet\'s mother. Hamlet is considered among the most powerful and influential works of 
world literature, with a story capable of "seemingly endless retelling and adaptation by others". It was one of Shakespeare\'s 
most popular works during his lifetime and still ranks among his most performed, topping the performance list of the Royal 
Shakespeare Company and its predecessors in Stratford-upon-Avon since 1879. It has inspired many other writers—from Johann 
Wolfgang von Goethe and Charles Dickens to James Joyce and Iris Murdoch—and has been described as "the world\'s most filmed 
story after Cinderella". The story of Shakespeare\'s Hamlet was derived from the legend of Amleth, preserved by 13th-century 
chronicler Saxo Grammaticus in his Gesta Danorum, as subsequently retold by the 16th-century scholar François de Belleforest. 
Shakespeare may also have drawn on an earlier Elizabethan play known today as the Ur-Hamlet, though some scholars believe 
Shakespeare wrote the Ur-Hamlet, later revising it to create the version of Hamlet that exists today. He almost certainly wrote 
his version of the title role for his fellow actor, Richard Burbage, the leading tragedian of Shakespeare\'s time. In the 400 
years since its inception, the role has been performed by numerous highly acclaimed actors in each successive century. Three 
different early versions of the play are extant: the First Quarto (Q1, 1603); the Second Quarto (Q2, 1604); and the First Folio 
(F1, 1623). Each version includes lines and entire scenes missing from the others. The play\'s structure and depth of 
characterisation have inspired much critical scrutiny. One such example is the centuries-old debate about Hamlet\'s hesitation 
to kill his uncle, which some see as merely a plot device to prolong the action but which others argue is a dramatisation of the 
complex philosophical and ethical issues that surround cold-blooded murder, calculated revenge, and thwarted desire. More 
recently, psychoanalytic critics have examined Hamlet\'s unconscious desires, while feminist critics have re-evaluated and 
attempted to rehabilitate the often-maligned characters of Ophelia and Gertrude.', 'Hamlet.jpg'),
('The Odyssey', 'Homer', 725, 'Poetry', 'The Odyssey is one of two major ancient Greek epic poems attributed to Homer. It is one 
of the oldest extant works of literature still read by contemporary audiences. As with the Iliad, the poem is divided into 24 
books. It follows the Greek hero Odysseus, king of Ithaca, and his journey home after the Trojan War. After the war itself, 
which lasted ten years, his journey lasts for ten additional years, during which time he encounters many perils and all his 
crewmates are killed. In his absence, Odysseus is assumed dead, and his wife Penelope and son Telemachus must contend with a 
group of unruly suitors who compete for Penelope\'s hand in marriage. The Odyssey was originally composed in Homeric Greek in 
around the 8th or 7th century BCE and, by the mid-6th century BCE, had become part of the Greek literary canon. In antiquity, 
Homer\'s authorship of the poem was not questioned, but contemporary scholarship predominantly assumes that the Iliad and the 
Odyssey were composed independently, and the stories themselves formed as part of a long oral tradition. Given widespread 
illiteracy, the poem was performed by an aoidos or rhapsode, and more likely to be heard than read. Crucial themes in the poem 
include the ideas of nostos, wandering, testing, and omens. Scholars still 
reflect on the narrative significance of certain groups in the poem, such as women and slaves, who have a more prominent role in 
the epic than in many other works of ancient literature. This focus is especially remarkable when considered beside the Iliad, 
which centres the exploits of soldiers and kings during the Trojan War. The Odyssey is regarded as one of the most significant 
works of the Western canon. The first English translation of the Odyssey was in the 16th century. Adaptations and re-imaginings 
continue to be produced across a wide variety of mediums. In 2018, when BBC Culture polled experts around the world to find 
literature\'s most enduring narrative, the Odyssey topped the list.','TheOdyssey.jpg'),
('Madame Bovary', 'Gustave Flaubert', 1856, 'Novel', 'Madame Bovary, originally published as Madame Bovary: Provincial Manners, 
is the debut novel of French writer Gustave Flaubert, published in 1856. The eponymous character lives beyond her means in order 
to escape the banalities and emptiness of provincial life. When the novel was first serialized in Revue de Paris between 1 
October 1856 and 15 December 1856, public prosecutors attacked the novel for obscenity. The resulting trial in January 1857 made 
the story notorious. After Flaubert\'s acquittal on 7 February 1857, Madame Bovary became a bestseller in April 1857 when it was 
published in two volumes. A seminal work of literary realism, the novel is now considered Flaubert\'s masterpiece, and one of the 
most influential literary works in history. The British critic James Wood writes: "Flaubert established, for good or ill, what 
most readers think of as modern realist narration, and his influence is almost too familiar to be visible."', 'MadameBovary.jpg'),
('The Divine Comedy', 'Dante Alighieri', 1465, 'Poetry', 'The Divine Comedy is a long Italian narrative poem by Dante Alighieri, 
begun c. 1308 and completed in 1320, a year before his death in 1321. It is widely considered to be the pre-eminent work in 
Italian literature and one of the greatest works of world literature. The poem\'s imaginative vision of the afterlife is 
representative of the medieval world-view as it had developed in the Western Church by the 14th century. It helped establish 
the Tuscan language, in which it is written, as the standardized Italian language. It is divided into three parts: Inferno, 
Purgatorio, and Paradiso. The narrative takes as its literal subject the state of the soul after death and presents an image of 
divine justice meted out as due punishment or reward, and describes Dante\'s travels through Hell, Purgatory, and Paradise or 
Heaven. Allegorically the poem represents the soul\'s journey towards God, beginning with the recognition and rejection of sin 
(Inferno), followed by the penitent Christian life (Purgatorio), which is then followed by the soul\'s ascent to God (Paradiso). 
Dante draws on medieval Roman Catholic theology and philosophy, especially Thomistic philosophy derived from the Summa 
Theologica of Thomas Aquinas. Consequently, the Divine Comedy has been called "the Summa in verse". In Dante\'s work, the 
pilgrim Dante is accompanied by three guides: Virgil (who represents human reason), Beatrice (who represents divine revelation, 
theology, faith, and grace), and Saint Bernard of Clairvaux (who represents contemplative mysticism and devotion to Mary the 
Mother). Erich Auerbach said Dante was the first writer to depict human beings as the products of a specific time, place and 
circumstance as opposed to mythic archetypes or a collection of vices and virtues; this along with the fully imagined world of 
the Divine Comedy, different from our own but fully visualized, suggests that the Divine Comedy could be said to have inaugurated 
modern fiction. The work was originally simply titled Comedìa also in the first printed edition, published in 1472, Tuscan for 
"Comedy", later adjusted to the modern Italian Commedia. The adjective Divina was added by Giovanni Boccaccio, due to its subject 
matter and lofty style, and the first edition to name the poem Divina Comedia in the title was that of the Venetian humanist 
Lodovico Dolce, published in 1555 by Gabriele Giolito de\' Ferrari.', 'TheDivineComedy.jpg'),
('Lolita', 'Vladimir Nabokov', 1955, 'Novel', 'Lolita is a 1955 novel written by Russian-American novelist Vladimir Nabokov. The 
novel is notable for its controversial subject: the protagonist and unreliable narrator, a middle-aged literature professor under 
the pseudonym Humbert Humbert, is obsessed with a 12-year-old girl, Dolores Haze, with whom he becomes sexually involved after he 
becomes her stepfather. "Lolita" is his private nickname for Dolores. The novel was originally written in English and first published 
in Paris in 1955 by Olympia Press. Later it was translated into Russian by Nabokov himself and published in New York City in 1967 
by Phaedra Publishers. Lolita quickly attained a classic status. The novel was adapted into a film by Stanley Kubrick in 1962, and 
another film by Adrian Lyne in 1997. It has also been adapted several times for the stage and has been the subject of two operas, 
two ballets, and an acclaimed, but commercially unsuccessful, Broadway musical. Many authors consider it the greatest work of the 
20th century,[2] and it has been included in several lists of best books, such as Time\'s List of the 100 Best Novels, Le Monde\'s 
100 Books of the Century, Bokklubben World Library, Modern Library\'s 100 Best Novels, and The Big Read.', 'Lolita.jpg'),
('The Brothers Karamazov', 'Fyodor Dostoyevsky', 1880, 'Novel', 'The Brothers Karamazov, also translated as The Karamazov Brothers, 
is the last novel by Russian author Fyodor Dostoevsky. Dostoevsky spent nearly two years writing The Brothers Karamazov, which 
was published as a serial in The Russian Messenger from January 1879 to November 1880. Dostoevsky died less than four months 
after its publication. Set in 19th-century Russia, The Brothers Karamazov is a passionate philosophical novel that enters deeply 
into questions of God, free will, and morality. It is a theological drama dealing with problems of faith, doubt and reason in the 
context of a modernizing Russia, with a plot that revolves around the subject of patricide. Dostoevsky composed much of the novel 
in Staraya Russa, which inspired the main setting. It has been acclaimed as one of the supreme achievements in literature.',
'TheBrothersKaramazov.jpg'),
('Crime and Punishment', 'Fyodor Dostoyevsky', 1867, 'Novel', 'Crime and Punishment is a novel by the Russian author Fyodor Dostoevsky. 
It was first published in the literary journal The Russian Messenger in twelve monthly installments during 1866. It was later 
published in a single volume. It is the second of Dostoevsky\'s full-length novels following his return from ten years of exile 
in Siberia. Crime and Punishment is considered the first great novel of his "mature" period of writing. The novel is often cited 
as one of the supreme achievements in literature. Crime and Punishment focuses on the mental anguish and moral dilemmas of 
Rodion Raskolnikov, an impoverished ex-student in Saint Petersburg who formulates a plan to kill an unscrupulous pawnbroker for 
her money. Before the killing, Raskolnikov believes that with the money he could liberate himself from poverty and go on to perform 
great deeds. However, once it is done he finds himself racked with confusion, paranoia, and disgust for his actions. His justifications 
disintegrate completely as he struggles with guilt and horror and confronts the real-world consequences of his deed.', 'CrimeAndPunishment.jpg'),
('Wuthering Heights', 'Emily Brontë', 1847, 'Tragedy', 'Wuthering Heights is an 1847 novel by Emily Brontë, published under the pseudonym 
Ellis Bell. It concerns two families of the landed gentry living on the West Yorkshire moors, the Earnshaws and the Lintons, and 
their turbulent relationships with Earnshaw\'s adopted son, Heathcliff. It was influenced by Romanticism and Gothic fiction. 
Wuthering Heights is now considered a classic of English literature, but contemporaneous reviews were polarised. It was 
controversial for its depictions of mental and physical cruelty, and for its challenges to Victorian morality, and religious and 
societal values. Wuthering Heights was accepted by publisher Thomas Newby along with Anne Brontë\'s Agnes Grey before the success 
of their sister Charlotte\'s novel Jane Eyre, but they were published later. Charlotte edited a second edition of Wuthering 
Heights after Emily\'s death which was published in 1850. It has inspired an array of adaptations across several media, including 
a hit song.', 'WutheringHeights.jpg'),
('The Catcher in the Rye', 'J. D. Salinger', 1945, 'Novel', 'The Catcher in the Rye is a novel by J. D. Salinger, partially published 
in serial form in 1945–1946 and as a novel in 1951. It was originally intended for adults but is often read by adolescents for 
its themes of angst, alienation, and as a critique on superficiality in society. It has been translated widely. About one million 
copies are sold each year, with total sales of more than 65 million books. The novel\'s protagonist Holden Caulfield has become 
an icon for teenage rebellion. The novel also deals with complex issues of innocence, identity, belonging, loss, connection, sex, 
and depression. The novel was included on Time Magazine\'s 2005 list of the 100 best English-language novels written since 1923, 
and it was named by Modern Library and its readers as one of the 100 best English-language novels of the 20th century. In 2003, 
it was listed at number 15 on the BBC\'s survey The Big Read.', 'TheCatcherInTheRye.jpg'),
('Pride and Prejudice', 'Jane Austen', 1813, 'Novel', 'Pride and Prejudice is an 1813 romantic novel of manners written by Jane Austen. 
The novel follows the character development of Elizabeth Bennet, the dynamic protagonist of the book who learns about the 
repercussions of hasty judgments and comes to appreciate the difference between superficial goodness and actual goodness. Its 
humour lies in its honest depiction of manners, education, marriage, and money during the Regency era in England. Mr. Bennet of 
Longbourn estate has five daughters, but his property is entailed and can only be passed to a male heir. His wife also lacks an 
inheritance, so his family will be destitute upon his death. Thus, it is imperative that at least one of the girls marry well to 
support the others, which is a motivation that drives the plot. The novel revolves around the importance of marrying for love 
rather than money or social prestige, despite the communal pressure to make a wealthy match. Pride and Prejudice has consistently 
appeared near the top of lists of "most-loved books" among literary scholars and the reading public. It has become one of the most 
popular novels in English literature, with over 20 million copies sold, and has inspired many derivatives in modern literature. 
For more than a century, dramatic adaptations, reprints, unofficial sequels, films, and TV versions of Pride and Prejudice have 
portrayed the memorable characters and themes of the novel, reaching mass audiences.', 'PrideAndPrejudice.jpg'),
('The Adventures of Huckleberry Finn', 'Mark Twain', 1885, 'Novel', 'Adventures of Huckleberry Finn (or, as it is known in more 
recent editions, The Adventures of Huckleberry Finn) is a novel by American author Mark Twain, which was first published in the 
United Kingdom in December 1884 and in the United States in February 1885. Commonly named among the Great American Novels, the 
work is among the first in major American literature to be written throughout in vernacular English, characterized by local color 
regionalism. It is told in the first person by Huckleberry "Huck" Finn, the narrator of two other Twain novels (Tom Sawyer Abroad 
and Tom Sawyer, Detective) and a friend of Tom Sawyer. It is a direct sequel to The Adventures of Tom Sawyer. The book is noted 
for its colorful description of people and places along the Mississippi River. Set in a Southern antebellum society that had ceased 
to exist over 20 years before the work was published, Adventures of Huckleberry Finn is an often scathing satire on entrenched 
attitudes, particularly racism. Perennially popular with readers, Adventures of Huckleberry Finn has also been the continued 
object of study by literary critics since its publication. The book was widely criticized upon release because of its extensive 
use of coarse language. Throughout the 20th century, and despite arguments that the protagonist and the tenor of the book are 
anti-racist,[2][3] criticism of the book continued due to both its perceived use of racial stereotypes and its frequent use of 
the racial slur "nigger".', 'TheAdventuresOfHuckleberryFinn.jpg'),
('Anna Karenina', 'Leo Tolstoy', 1878, 'Novel', 'Anna Karenina is a novel by the Russian author Leo Tolstoy, first published 
in book form in 1878. Many writers consider it the greatest work of literature ever written, and Tolstoy himself called it his 
first true novel. It was initially released in serial installments from 1873 to 1877 in the periodical The Russian Messenger. 
A complex novel in eight parts, with more than a dozen major characters, Anna Karenina is spread over more than 800 pages 
(depending on the translation and publisher), typically contained in two volumes. It deals with themes of betrayal, faith, 
family, marriage, Imperial Russian society, desire, and rural vs. city life. The story centers on an extramarital affair between 
Anna and dashing cavalry officer Count Alexei Kirillovich Vronsky that scandalizes the social circles of Saint Petersburg and 
forces the young lovers to flee to Italy in a search for happiness, but after they return to Russia, their lives further unravel. 
Trains are a recurring motif throughout the novel, with several major plot points taking place either on passenger trains or at 
stations in Saint Petersburg or elsewhere in Russia. The story takes place against the backdrop of the liberal reforms initiated 
by Emperor Alexander II of Russia and the rapid societal transformations that followed. The novel has been adapted into various 
media including theatre, opera, film, television, ballet, figure skating, and radio drama.', 'AnnaKarenina.jpg'),
('Alices Adventures in Wonderland', 'Lewis Carroll', 1865, 'Fantasy', 'Alices Adventures in Wonderland (commonly shortened to 
Alice in Wonderland) is an 1865 novel by English author Lewis Carroll (the pseudonym  of Charles Dodgson). It tells of a young 
girl named Alice, who falls through a rabbit hole into a subterranean fantasy world populated by peculiar, anthropomorphic 
creatures. It is considered to be one of the best examples of the literary nonsense genre. The tale plays with logic, giving 
the story lasting popularity with adults as well as with children. One of the best-known and most popular works of English-
language fiction, its narrative, structure, characters and imagery have been enormously influential in popular culture and 
literature, especially in the fantasy genre. The work has never been out of print and has been translated into at least 97 
languages. Its ongoing legacy encompasses many adaptations for stage, screen, radio, art, ballet, theme parks, board games 
and video games. Carroll published a sequel in 1871, titled Through the Looking-Glass, and a shortened version for young 
children, The Nursery "Alice", in 1890.', 'AlicesAdventuresInWonderland.jpg'),
('The Iliad', 'Homer', 762, 'Poetry', 'The Iliad is an ancient Greek epic poem in dactylic hexameter, traditionally attributed 
to Homer. Usually considered to have been written down circa the 8th century BC, the Iliad is among the oldest extant works of 
Western literature, along with the Odyssey, another epic poem attributed to Homer which tells of Odysseus\'s experiences after 
the events of the Iliad. In the modern vulgate (the standard accepted version), the Iliad contains 15,693 lines, divided into 
24 books; it is written in Homeric Greek, a literary amalgam of Ionic Greek and other dialects. It is usually grouped in the 
Epic Cycle. Set during the Trojan War, the ten-year siege of the city of Troy (Ilium) by a coalition of Mycenaean Greek states 
(Achaeans), it tells of the battles and events during the weeks of a quarrel between King Agamemnon and the warrior Achilles. 
Although the story covers only a few weeks in the final year of the war, the Iliad mentions or alludes to many of the Greek 
legends about the siege; the earlier events, such as the gathering of warriors for the siege, the cause of the war, and related 
concerns tend to appear near the beginning. Then the epic narrative takes up events prophesied for the future, such as Achilles\' 
imminent death and the fall of Troy, although the narrative ends before these events take place. However, as these events are 
prefigured and alluded to more and more vividly, when it reaches an end the poem has told a more or less complete tale of the 
Trojan War.', 'TheIliad.jpg'),
('To the Lighthouse', 'Virginia Woolf', 1927, 'Modernism', 'To the Lighthouse is a 1927 novel by Virginia Woolf. The novel 
centers on the Ramsay family and their visits to the Isle of Skye in Scotland between 1910 and 1920. Following and extending 
the tradition of modernist novelists like Marcel Proust and James Joyce, the plot of To the Lighthouse is secondary to its 
philosophical introspection. Cited as a key example of the literary technique of multiple focalization, the novel includes 
little dialogue and almost no direct action; most of it is written as thoughts and observations. The novel recalls childhood 
emotions and highlights adult relationships. Among the book\'s many tropes and themes are those of loss, subjectivity, the 
nature of art and the problem of perception. In 1998, the Modern Library named To the Lighthouse No. 15 on its list of the 100 
best English-language novels of the 20th century. In 2005, the novel was chosen by TIME magazine as one of the one hundred best 
English-language novels since 1923.', 'ToTheLighthouse.jpg'),
('Catch-22', 'Joseph Heller', 1961, 'Novel', 'Catch-22 is a satirical war novel by American author Joseph Heller. He began writing 
it in 1953 - the novel was first published in 1961. Often cited as one of the most significant novels of the twentieth century, 
it uses a distinctive non-chronological third-person omniscient narration, describing events from the points of view of different 
characters. The separate storylines are out of sequence so the timeline develops along with the plot. The novel is set during 
World War II, from 1942 to 1944. It mainly follows the life of antihero Captain John Yossarian, a U.S. Army Air Forces B-25 
bombardier. Most of the events in the book occur while the fictional 256th US Army Air Squadron is based on the island of 
Pianosa, in the Mediterranean Sea west of Italy, though it also covers episodes from basic training at Lowry Field in Colorado 
and Air Corps training at Santa Ana Army Air Base in California. The novel examines the absurdity of war and military life 
through the experiences of Yossarian and his cohorts, who attempt to maintain their sanity while fulfilling their service 
requirements so that they may return home. The book was made into a film adaptation in 1970, directed by Mike Nichols. In 1994, 
Heller published a sequel to the 1961 novel entitled Closing Time.', 'Catch-22.jpg'),
('Heart of Darkness', 'Joseph Conrad', 1899, 'Novella', 'Heart of Darkness (1899) is a novella by Polish-English novelist Joseph 
Conrad about a narrated voyage up the Congo River into the Congo Free State in the Heart of Africa. Charles Marlow, the narrator, 
tells his story to friends aboard a boat anchored on the River Thames. This setting provides the frame for Marlow\'s story of his 
obsession with the successful ivory trader Kurtz. Conrad offers parallels between London ("the greatest town on earth") and 
Africa as places of darkness. Central to Conrad\'s work is the idea that there is little difference between "civilised people" 
and "savages." Heart of Darkness implicitly comments on imperialism and racism. Originally issued as a three-part serial story 
in Blackwood\'s Magazine to celebrate the thousandth edition of the magazine, Heart of Darkness has been widely re-published 
and translated into many languages. It provided the inspiration for Francis Ford Coppola\'s 1979 film Apocalypse Now. In 1998, 
the Modern Library ranked Heart of Darkness 67th on their list of the 100 best novels in English of the twentieth century.',
'HeartOfDarkness.jpg'),
('The Sound and the Fury', 'William Faulkner', 1929, 'Novel', 'The Sound and the Fury is a novel by the American author William 
Faulkner. It employs several narrative styles, including stream of consciousness. Published in 1929, The Sound and the Fury was 
Faulkner\'s fourth novel, and was not immediately successful. In 1931, however, when Faulkner\'s sixth novel, Sanctuary, was 
published — a sensationalist story, which Faulkner later said was written only for money—The Sound and the Fury also became 
commercially successful, and Faulkner began to receive critical attention. In 1998, the Modern Library ranked The Sound and the 
Fury sixth on its list of the 100 best English-language novels of the 20th century.', 'TheSoundAndTheFury.jpg'),
('Nineteen Eighty Four', 'George Orwell', 1949, 'Fiction', 'Nineteen Eighty-Four: A Novel, often referred to as 1984, is a dystopian 
social science fiction novel by English novelist George Orwell. It was published on 8 June 1949 by Secker & Warburg as Orwell\'s 
ninth and final book completed in his lifetime. Thematically, Nineteen Eighty-Four centres on the consequences of totalitarianism, 
mass surveillance, and repressive regimentation of persons and behaviours within society. Orwell, himself a democratic socialist, 
modelled the authoritarian government in the novel after Stalinist Russia. More broadly, the novel examines the role of truth and 
facts within politics and the ways in which they are manipulated. The story takes place in an imagined future, the year 1984, 
when much of the world has fallen victim to perpetual war, omnipresent government surveillance, historical negationism, and 
propaganda. Great Britain, known as Airstrip One, has become a province of a totalitarian superstate named Oceania that is 
ruled by the Party who employ the Thought Police to persecute individuality and independent thinking. Big Brother, the leader 
of the Party, enjoys an intense cult of personality despite the fact that he may not even exist. The protagonist, Winston Smith, 
is a diligent and skillful rank-and-file worker and Outer Party member who secretly hates the Party and dreams of rebellion. He 
enters into a forbidden relationship with a colleague, Julia, and starts to remember what life was like before the Party came to 
power. Nineteen Eighty-Four has become a classic literary example of political and dystopian fiction. It also popularised the 
term "Orwellian" as an adjective, with many terms used in the novel entering common usage, including "Big Brother", "doublethink", 
"Thought Police", "thoughtcrime", "Newspeak", "memory hole", "2 + 2 = 5", "proles", "Two Minutes Hate", "telescreen", and 
"Room 101". Time included it on its 100 best English-language novels from 1923 to 2005. It was placed on the Modern Library\'s 
100 Best Novels, reaching No. 13 on the editors\' list and No. 6 on the readers\' list. In 2003, the novel was listed at No. 8 on 
The Big Read survey by the BBC. Parallels have been drawn between the novel\'s subject matter and real life instances of 
totalitarianism, mass surveillance, and violations of freedom of expression among other themes.', 'NineteenEightyFour.jpg'),
('Great Expectations', 'Charles Dickens', 1861, 'Novel', 'Great Expectations is the thirteenth novel by Charles Dickens and his 
penultimate completed novel. It depicts the education of an orphan nicknamed Pip (the book is a bildungsroman, a coming-of-age 
story). It is Dickens\'s second novel, after David Copperfield, to be fully narrated in the first person. The novel was first 
published as a serial in Dickens\'s weekly periodical All the Year Round, from 1 December 1860 to August 1861. In October 1861, 
Chapman and Hall published the novel in three volumes. The novel is set in Kent and London in the early to mid-19th century and 
contains some of Dickens\'s most celebrated scenes, starting in a graveyard, where the young Pip is accosted by the escaped 
convict Abel Magwitch.Great Expectations is full of extreme imagery – poverty, prison ships and chains, and fights to the death 
– and has a colourful cast of characters who have entered popular culture. These include the eccentric Miss Havisham, the 
beautiful but cold Estella, and Joe, the unsophisticated and kind blacksmith. Dickens\'s themes include wealth and poverty, love 
and rejection, and the eventual triumph of good over evil. Great Expectations, which is popular both with readers and literary 
critics, has been translated into many languages and adapted numerous times into various media. Upon its release, the novel 
received near universal acclaim. Although Dickens\'s contemporary Thomas Carlyle referred to it disparagingly as that "Pip 
nonsense," he nevertheless reacted to each fresh installment with "roars of laughter." Later, George Bernard Shaw praised the 
novel, as "All of one piece and consistently truthful." During the serial publication, Dickens was pleased with public response 
to Great Expectations and its sales; when the plot first formed in his mind, he called it "a very fine, new and grotesque idea." 
In the 21st century, the novel retains good ratings among literary critics and in 2003 it was ranked 17th on the BBC\'s The Big 
Read poll.', 'GreatExpectations.jpg'),
('One Thousand and One Nights', 'India/Iran/Iraq/Egypt',1721, 'Folk Tales', 'One Thousand and One Nights is a collection of Middle 
Eastern folk tales compiled in Arabic during the Islamic Golden Age. It is often known in English as the Arabian Nights, from 
the first English-language edition (c. 1706–1721), which rendered the title as The Arabian Nights\' Entertainment. The work was 
collected over many centuries by various authors, translators, and scholars across West, Central and South Asia, and North Africa. 
Some tales themselves trace their roots back to ancient and medieval Arabic, Egyptian, Indian, Persian, and Mesopotamian folklore 
and literature. In particular, many tales were originally folk stories from the Abbasid and Mamluk eras, while others, especially 
the frame story, are most probably drawn from the Pahlavi Persian work, which in turn relied partly on Indian elements. 
What is common to all the editions of the Nights is the initial frame story of the ruler Shahryar and his wife Scheherazade and 
the framing device incorporated throughout the tales themselves. The stories proceed from this original tale; some are framed 
within other tales, while others are self-contained. Some editions contain only a few hundred nights, while others include 1,001 
or more. The bulk of the text is in prose, although verse is occasionally used for songs and riddles and to express heightened 
emotion. Most of the poems are single couplets or quatrains, although some are longer. Some of the stories commonly associated 
with the Arabian Nights—particularly "Aladdin\'s Wonderful Lamp" and "Ali Baba and the Forty Thieves"—were not part of the 
collection in its original Arabic versions but were added to the collection by Antoine Galland after he heard them from the 
Syrian Maronite Christian storyteller Hanna Diab on Diab\'s visit to Paris. Other stories, such as "The Seven Voyages of Sinbad 
the Sailor", had an independent existence before being added to the collection.', 'OneThousandAndOneNights.jpg'),
('The Grapes of Wrath', 'John Steinbeck', 1939, 'Novel', 'The Grapes of Wrath is an American realist novel written by John 
Steinbeck and published in 1939. The book won the National Book Award and Pulitzer Prize for fiction, and it was cited 
prominently when Steinbeck was awarded the Nobel Prize in 1962. Set during the Great Depression, the novel focuses on the Joads, 
a poor family of tenant farmers driven from their Oklahoma home by drought, economic hardship, agricultural industry changes, 
and bank foreclosures forcing tenant farmers out of work. Due to their nearly hopeless situation, and in part because they are 
trapped in the Dust Bowl, the Joads set out for California along with thousands of other "Okies" seeking jobs, land, dignity, 
and a future. The Grapes of Wrath is frequently read in American high school and college literature classes due to its 
historical context and enduring legacy. A celebrated Hollywood film version, starring Henry Fonda and directed by John Ford, 
was released in 1940.', 'TheGrapesOfWrath.jpg'),
('Absalom, Absalom!', 'William Faulkner', 1936, 'Novel', 'Absalom, Absalom! is a novel by the American author William Faulkner, 
first published in 1936. Taking place before, during, and after the American Civil War, it is a story about three families of 
the American South, with a focus on the life of Thomas Sutpen. Absalom, Absalom! details the rise and fall of Thomas Sutpen, a 
white man born into poverty in West Virginia who moves to Mississippi with the complementary aims of gaining wealth and becoming 
a powerful family patriarch. The story is told entirely in flashbacks narrated mostly by Quentin Compson to his roommate at 
Harvard University, Shreve, who frequently contributes his own suggestions and surmises. The narration of Rosa Coldfield, and 
Quentin\'s father and grandfather, are also included and re-interpreted by Shreve and Quentin, with the total events of the story 
unfolding in nonchronological order and often with differing details. This results in a peeling-back-the-onion revelation of the 
true story of the Sutpens. Rosa initially narrates the story, with long digressions and a biased memory, to Quentin Compson, 
whose grandfather was a friend of Sutpen\'s. Quentin\'s father then fills in some of the details to Quentin. Finally, Quentin 
relates the story to his roommate Shreve, and in each retelling, the reader receives more details as the parties flesh out the 
story by adding layers. The final effect leaves the reader more certain about the attitudes and biases of the characters than 
about the facts of Sutpen\'s story.', 'Absalom,Absalom!.jpg'),
('Invisible Man', 'Ralph Ellison', 1952, 'Bildungsroman', 'Invisible Man is a novel by Ralph Ellison, published by Random House 
in 1952. It addresses many of the social and intellectual issues faced by African Americans in the early twentieth century, 
including black nationalism, the relationship between black identity and Marxism, and the reformist racial policies of Booker 
T. Washington, as well as issues of individuality and personal identity. Invisible Man won the U.S. National Book Award for 
Fiction in 1953, making Ellison the first African American writer to win the award. In 1998, the Modern Library ranked Invisible 
Man 19th on its list of the 100 best English-language novels of the 20th century. Time magazine included the novel in its TIME 
100 Best English-language Novels from 1923 to 2005, calling it "the quintessential American picaresque of the 20th century," 
rather than a "race novel, or even a bildungsroman." Malcolm Bradbury and Richard Ruland recognize an existential vision with 
a "Kafka-like absurdity." According to The New York Times, Barack Obama modeled his 1995 memoir Dreams from My Father on 
Ellison\'s novel.', 'InvisibleMan.jpg'),
('To Kill a Mockingbird', 'Harper Lee', 1960, 'Bildungsroman', 'To Kill a Mockingbird is a novel by the American author Harper 
Lee. It was published in 1960 and was instantly successful. In the United States, it is widely read in high schools and middle 
schools. To Kill a Mockingbird has become a classic of modern American literature, winning the Pulitzer Prize. The plot and 
characters are loosely based on Lee\'s observations of her family, her neighbors and an event that occurred near her hometown 
of Monroeville, Alabama, in 1936, when she was ten. Despite dealing with the serious issues of rape and racial inequality, the 
novel is renowned for its warmth and humor. Atticus Finch, the narrator\'s father, has served as a moral hero for many readers 
and as a model of integrity for lawyers. The historian Joseph Crespino explains, "In the twentieth century, To Kill a Mockingbird 
is probably the most widely read book dealing with race in America, and its main character, Atticus Finch, the most enduring 
fictional image of racial heroism." As a Southern Gothic and Bildungsroman novel, the primary themes of To Kill a Mockingbird 
involve racial injustice and the destruction of innocence. Scholars have noted that Lee also addresses issues of class, courage, 
compassion, and gender roles in the Deep South. The book is widely taught in schools in the United States with lessons that 
emphasize tolerance and decry prejudice. Despite its themes, To Kill a Mockingbird has been subject to campaigns for removal 
from public classrooms, often challenged for its use of racial epithets. In 2006, British librarians ranked the book ahead of 
the Bible as one "every adult should read before they die". Reaction to the novel varied widely upon publication. Despite the 
number of copies sold and its widespread use in education, literary analysis of it is sparse. Author Mary McDonough Murphy, who 
collected individual impressions of To Kill a Mockingbird by several authors and public figures, calls the book "an astonishing 
phenomenon". It was adapted into an Academy Award-winning film in 1962 by director Robert Mulligan, with a screenplay by Horton 
Foote. Since 1990, a play based on the novel has been performed annually in Harper Lee\'s hometown. To Kill a Mockingbird was 
Lee\'s only published book until Go Set a Watchman, an earlier draft of To Kill a Mockingbird, was published on July 14, 2015. 
Lee continued to respond to her work\'s impact until her death in February 2016, although she had refused any personal publicity 
for herself or the novel since 1964.', 'ToKillAMockingbird.jpg'),
('The Trial', 'Franz Kafka', 1914, 'Novel', 'The Trial is a novel written by Franz Kafka between 1914 and 1915 and published 
posthumously on 26 April 1925. One of his best-known works, it tells the story of Josef K., a man arrested and prosecuted by a 
remote, inaccessible authority, with the nature of his crime revealed neither to him nor to the reader. Heavily influenced by 
Dostoevsky\'s Crime and Punishment and The Brothers Karamazov, Kafka even went so far as to call Dostoevsky a blood relative. 
Like Kafka\'s other novels, The Trial was never completed, although it does include a chapter which appears to bring the story 
to an intentionally abrupt ending. After Kafka\'s death in 1924 his friend and literary executor Max Brod edited the text for 
publication by Verlag Die Schmiede. The original manuscript is held at the Museum of Modern Literature, Marbach am Neckar, 
Germany. The first English-language translation, by Willa and Edwin Muir, was published in 1937. In 1999, the book was listed 
in Le Monde\'s 100 Books of the Century and as No. 2 of the Best German Novels of the Twentieth Century.', 'TheTrial.jpg'),
('The Red and the Black', 'Stendhal', 1830, 'Novel', 'The Red and the Black is a historical psychological novel in two volumes 
by Stendhal, published in 1830. It chronicles the attempts of a provincial young man to rise socially beyond his modest 
upbringing through a combination of talent, hard work, deception, and hypocrisy. He ultimately allows his passions to betray 
him. The novel\'s full title, Le Rouge et le Noir: Chronique du XIXe siècle  indicates its twofold literary purpose as both a 
psychological portrait of the romantic protagonist, Julien Sorel, and an analytic, sociological satire of the French social 
order under the Bourbon Restoration (1814–30). In English, Le Rouge et le Noir is variously translated as Red and Black, Scarlet 
and Black, and The Red and the Black, without the subtitle. The title is taken to refer to the tension between the clerical 
(black) and secular (red) interests of the protagonist, but this interpretation is but one of many.', 'TheRedAndTheBlack.jpg'),
('Middlemarch', 'George Eliot', 1872, 'Novel', 'Middlemarch, A Study of Provincial Life is a novel by the English author Mary 
Anne Evans, who wrote as George Eliot. It first appeared in eight instalments (volumes) in 1871 and 1872. Set in Middlemarch, a 
fictional English Midland town in 1829 to 1832, it follows distinct, intersecting stories with many characters. Issues include 
the status of women, the nature of marriage, idealism, self-interest, religion, hypocrisy, political reform, and education. 
Despite comic elements, Middlemarch uses realism to encompass historical events: the 1832 Reform Act, early railways, and the 
accession of King William IV. It looks at medicine of the time and reactionary views in a settled community facing unwelcome 
change. Eliot began writing the two pieces that formed the novel in 1869–1870 and completed it in 1871. Initial reviews were 
mixed, but it is now seen widely as her best work and one of the great English novels.', 'Middlemarch.jpg'),
('Gullivers Travels', 'Jonathan Swift', 1726, 'Fantasy', 'Gullivers Travels, or Travels into Several Remote Nations of the World.
In Four Parts. By Lemuel Gulliver, First a Surgeon, and then a Captain of Several Ships is a 1726 prose satire by the Irish 
writer and clergyman Jonathan Swift, satirising both human nature and the "travellers\' tales" literary subgenre. It is Swift\'s 
best known full-length work, and a classic of English literature. Swift claimed that he wrote Gulliver\'s Travels "to vex the 
world rather than divert it". The book was an immediate success. The English dramatist John Gay remarked "It is universally read, 
from the cabinet council to the nursery." In 2015, Robert McCrum released his selection list of 100 best novels of all time in 
which Gulliver\'s Travels is listed as "a satirical masterpiece".', 'GulliversTravels.jpg'),
('Beloved', 'Toni Morrison', 1987, 'Novel', 'Beloved is a 1987 novel by the American writer Toni Morrison. Set after the American 
Civil War, it tells the story of a family of former slaves whose Cincinnati home is haunted by a malevolent spirit. Beloved is 
inspired by a true-life incident involving Margaret Garner, an escaped slave from Kentucky who fled to the free state of Ohio 
in 1856, but was captured in accordance with the Fugitive Slave Act of 1850. When U.S. marshals burst into the cabin where 
Garner and her husband had barricaded themselves, they found that she had killed her two-year-old daughter and was attempting 
to kill her other children to spare them from being returned to slavery. Morrison had come across an account of Garner titled 
"A Visit to the Slave Mother who Killed Her Child" in an 1856 newspaper article published in the American Baptist, and 
reproduced in The Black Book, a miscellaneous compilation of black history and culture that Morrison edited in 1974. The novel 
won the Pulitzer Prize for Fiction in 1988 and was a finalist for the 1987 National Book Award. It was adapted as a 1998 movie 
of the same name, starring Oprah Winfrey. A survey of writers and literary critics compiled by The New York Times ranked it as 
the best work of American fiction from 1981 to 2006.', 'Beloved.jpg'),
('Mrs. Dalloway', 'Virginia Woolf', 1925, 'Novel', 'Mrs. Dalloway (published on 14 May 1925) is a novel by Virginia Woolf that 
details a day in the life of Clarissa Dalloway, a fictional high-society woman in post–First World War England. It is one of 
Woolf\'s best-known novels. Created from two short stories, "Mrs Dalloway in Bond Street" and the unfinished "The Prime Minister", 
the novel addresses Clarissa\'s preparations for a party she will host that evening. With an interior perspective, the story 
travels forward and back in time and in and out of the characters\' minds to construct an image of Clarissa\'s life and of the 
inter-war social structure. In October 2005, Mrs. Dalloway was included on Time\'s list of the 100 best English-language novels 
written since Time debuted in 1923.', 'Mrs.Dalloway.jpg'),
('The Stories of Anton Chekhov', 'Anton Chekhov', 1932, 'Biography', 'Richard Pevear and Larissa Volokhonsky, the highly acclaimed 
translators of War and Peace, Doctor Zhivago, and Anna Karenina, which was an Oprah Book Club pick and million-copy bestseller, 
bring their unmatched talents to The Selected Stories of Anton Chekhov, a collection of thirty of Chekhov’s best tales from the 
major periods of his creative life. Considered the greatest short story writer, Anton Chekhov changed the genre itself with his 
spare, impressionistic depictions of Russian life and the human condition. From characteristically brief, evocative early pieces 
such as “The Huntsman” and the tour de force “A Boring Story,” to his best-known stories such as “The Lady with the Little Dog” 
and his own personal favorite, “The Student,” Chekhov’s short fiction possesses the transcendent power of art to awe and change 
the reader. This monumental edition, expertly translated, is especially faithful to the meaning of Chekhov’s prose and the unique 
rhythms of his writing, giving readers an authentic sense of his style and a true understanding of his greatness.', 
'TheStoriesOfAntonChekhov.jpg'),
('The Stranger', 'Albert Camus', 1946, 'Novel', 'The Stranger is a 1942 novella by French author Albert Camus. Its theme and outlook 
are often cited as examples of Camus\' philosophy, absurdism coupled with existentialism, though Camus personally rejected the 
latter label. The title character is Meursault, an indifferent French Algerian described as "a citizen of France domiciled in 
North Africa, a man of the Mediterranean, an homme du midi yet one who hardly partakes of the traditional Mediterranean culture." 
He attends his mother\'s funeral. Weeks later, he kills an Arab man in French Algiers, who was involved in a conflict with one 
of Meursault\'s neighbors. Meursault is tried and sentenced to death. The story is divided into two parts, presenting Meursault\'s 
first-person narrative view before and after the murder, respectively. In January 1955, Camus wrote this: I summarized The 
Stranger a long time ago, with a remark I admit was highly paradoxical: "In our society any man who does not weep at his mother\'s 
funeral runs the risk of being sentenced to death." I only meant that the hero of my book is condemned because he does not play 
the game. The Stranger\'s first edition consisted of only 4,400 copies, which was so few that it could not be a best-seller. 
Since the novella was published during the Nazi occupation of France, there was a possibility that the Propaganda-Staffel would 
censor it, but a representative of the Occupation authorities felt it contained nothing damaging to their cause, so it was 
published without omissions. However, the novel was well received in anti-Nazi circles in addition to Jean-Paul Sartre\'s article 
"Explication de LEtranger". Translated four times into English, and also into numerous other languages, the novel has long been 
considered a classic of 20th-century literature. Le Monde ranks it as number one on its 100 Books of the Century. The novel was 
twice adapted as films: Lo Straniero (1967) (Italian) by Luchino Visconti and Yazgi (2001, Fate) by Zeki Demirkubuz (Turkish).',
'TheStranger.jpg'),
('Jane Eyre', 'Charlotte Bronte', 1847, 'Novel', 'Jane Eyre (originally published as Jane Eyre: An Autobiography) is a novel by 
English writer Charlotte Brontë, published under the pen name "Currer Bell", on 16 October 1847, by Smith, Elder & Co. of London. 
The first American edition was published the following year by Harper & Brothers of New York.[1] Jane Eyre is a Bildungsroman 
which follows the experiences of its eponymous heroine, including her growth to adulthood and her love for Mr. Rochester, the 
brooding master of Thornfield Hall. The novel revolutionised prose fiction by being the first to focus on its protagonist\'s 
moral and spiritual development through an intimate first-person narrative, where actions and events are coloured by a 
psychological intensity. Charlotte Brontë has been called the "first historian of the private consciousness", and the literary 
ancestor of writers like Proust and Joyce. The book contains elements of social criticism with a strong sense of Christian 
morality at its core, and it is considered by many to be ahead of its time because of Jane\'s individualistic character and how 
the novel approaches the topics of class, sexuality, religion, and feminism. It, along with Jane Austen\'s Pride and Prejudice, 
is one of the most famous romance novels of all time.', 'JaneEyre.jpg'),
('The Aeneid', 'Virgil', 1697, 'Poetry', 'The Aeneid is a Latin epic poem, written by Virgil between 29 and 19 BC, that tells 
the legendary story of Aeneas, a Trojan who travelled to Italy, where he became the ancestor of the Romans. It comprises 9,896 
lines in dactylic hexameter. The first six of the poem\'s twelve books tell the story of Aeneas\'s wanderings from Troy to Italy, 
and the poem\'s second half tells of the Trojans\' ultimately victorious war upon the Latins, under whose name Aeneas and his 
Trojan followers are destined to be subsumed. The hero Aeneas was already known to Greco-Roman legend and myth, having been a 
character in the Iliad. Virgil took the disconnected tales of Aeneas\'s wanderings, his vague association with the foundation 
of Rome and his description as a personage of no fixed characteristics other than a scrupulous pietas, and fashioned the Aeneid 
into a compelling founding myth or national epic that tied Rome to the legends of Troy, explained the Punic Wars, glorified 
traditional Roman virtues, and legitimized the Julio-Claudian dynasty as descendants of the founders, heroes, and gods of Rome 
and Troy. The Aeneid is widely regarded as Virgil\'s masterpiece and one of the greatest works of Latin literature.', 
'TheAeneid.jpg'),
('Collected Fiction', 'Jorge Luis Borges', 1944, 'Fantasy', 'Jorge Luis Borges has been called the greatest Spanish-language 
writer of our century. Now for the first time in English, all of Borges\' dazzling fictions are gathered into a single volume, 
brilliantly translated by Andrew Hurley. From his 1935 debut with The Universal History of Iniquity, through his immensely 
influential collections Ficciones and The Aleph, these enigmatic, elaborate, imaginative inventions display Borges\' talent 
for turning fiction on its head by playing with form and genre and toying with language. Together these incomparable works 
comprise the perfect one-volume compendium for all those who have long loved Borges, and a superb introduction to the master\'s 
work for those who have yet to discover this singular genius.', 'CollectedFiction.jpg'),
('The Sun Also Rises', 'Ernest Hemingway', 1926, 'Novel', 'The Sun Also Rises is a 1926 novel by American writer Ernest Hemingway, 
his first, that portrays American and British expatriates who travel from Paris to the Festival of San Fermín in Pamplona to 
watch the running of the bulls and the bullfights. An early and enduring modernist novel, it received mixed reviews upon 
publication. However, Hemingway biographer Jeffrey Meyers writes that it is now "recognized as Hemingway\'s greatest work", 
and Hemingway scholar Linda Wagner-Martin calls it his most important novel. The novel was published in the United States in 
October 1926 by Scribner\'s. A year later, Jonathan Cape published the novel in London under the title Fiesta. It remains in 
print. The novel is a roman à clef: the characters are based on real people in Hemingway\'s circle, and the action is based 
on real events, particularly Hemingway\'s life in Paris in the 1920s and a trip to Spain in 1925 for the Pamplona festival and 
fishing in the Pyrenees. Hemingway presents his notion that the "Lost Generation"—considered to have been decadent, dissolute, 
and irretrievably damaged by World War I—was in fact resilient and strong. Hemingway investigates the themes of love and death, 
the revivifying power of nature, and the concept of masculinity. His spare writing style, combined with his restrained use of 
description to convey characterizations and action, demonstrates his "Iceberg Theory" of writing.', 'TheSunAlsoRises.jpg'),
('David Copperfield', 'Charles Dickens', 1850, 'Bildungsroman', 'The Personal History, Adventures, Experience and Observation of
David Copperfield the Younger of Blunderstone Rookery (Which He Never Meant to Publish on Any Account), commonly known as David 
Copperfield), is a bildungsroman by Charles Dickens, narrated by the eponymous David Copperfield, detailing his adventures in 
his journey from infancy to maturity. It was first published as a serial in 1849–50, and as a book in 1850. David Copperfield is 
also an autobiographical novel: "a very complicated weaving of truth and invention", with events following Dickens\'s own life. 
Of the books he wrote, it was his favourite. Called "the triumph of the art of Dickens", it marks a turning point in his work, 
separating the novels of youth and those of maturity. At first glance, the work is modelled on 18th-century "personal histories" 
that were very popular, like Henry Fielding\'s Joseph Andrews or Tom Jones, but David Copperfield is a more carefully structured 
work. It begins, like other novels by Dickens, with a bleak picture of childhood in Victorian England, followed by young 
Copperfield\'s slow social ascent, as he painfully provides for his aunt, while continuing his studies. Dickens wrote without 
an outline, unlike his previous novel, Dombey and Son. Some aspects of the story were fixed in his mind from the start, but 
others were undecided until the serial publications were underway. The novel has a primary theme of growth and change, but 
Dickens also satirises many aspects of Victorian life. This includes: the plight of prostitutes; the status of women in marriage; 
class structure; the criminal justice system; the quality of schools; and the employment of children in factories.', 'DavidCopperfield.jpg'),
('Tristram Shandy', 'Laurence Sterne', 1767, 'Novel', 'The Life and Opinions of Tristram Shandy, Gentleman, also known as just 
Tristram Shandy, is a novel by Laurence Sterne. It was published in nine volumes, the first two appearing in 1759, and seven 
others following over the next seven years (vols. 3 and 4, 1761; vols. 5 and 6, 1762; vols. 7 and 8, 1765; vol. 9, 1767). It 
purports to be a biography of the eponymous character. Its style is marked by digression, double entendre, and graphic devices. 
Sterne had read widely, which is reflected in Tristram Shandy. Many of his similes, for instance, are reminiscent of the works 
of the metaphysical poets of the 17th century, and the novel as a whole, with its focus on the problems of language, has 
constant regard for John Locke\'s theories in An Essay Concerning Human Understanding. Arthur Schopenhauer called Tristram 
Shandy one of "the four immortal romances."', 'TristramShandy.jpg'),
('Leaves of Grass', 'Walt Whitman', 1855, 'Poetry', '“Leaves of Grass” is a poetry collection by American poet Walt Whitman. 
First published in 1855, Whitman spent most of his professional life writing and rewriting Leaves of Grass, revising it multiple 
times until his death. There have been held to be either six or nine individual editions of Leaves of Grass; the count varying 
depending on how they are distinguished.] This resulted in vastly different editions over four decades—the first edition being a 
small book of twelve poems, and the last, a compilation of over 400. The collection of loosely-connected poems represents the 
celebration of his philosophy of life and humanity, and praises nature and the individual human\'s role in it. Rather than 
focusing on religious or spiritual matters, Leaves of Grass focuses primarily on the body and the material world. With one 
exception, its poems do not rhyme or follow standard rules for meter and line length. Leaves of Grass is regarded by many 
scholars as a completely "do-it-yourself" project. Whitman chose his idealized self as subject of the book, created the style in 
which it was written (working hard and intelligently to perfect the style over a period of six or seven years), and created the 
personality of the proletarian bard—the supposed writer of the poems. Leaves of Grass is also notable for its discussion of 
delight in sensual pleasures during a time when such candid displays were considered immoral. The book was highly controversial 
during its time for its explicit sexual imagery, and Whitman was subject to derision by many contemporary critics. Over time, 
however, the collection has infiltrated popular culture and became recognized as one of the central works of American poetry.', 
'LeavesOfGrass.jpg'),
('The Magic Mountain', 'Thomas Mann', 1924, 'Bildungsroman', 'The Magic Mountain is a novel by Thomas Mann, first published in 
German in November 1924. It is widely considered to be one of the most influential works of twentieth-century German literature. 
Mann started writing what was to become The Magic Mountain in 1912. It began as a much shorter narrative which revisited in a 
comic manner aspects of Death in Venice, a novella that he was preparing for publication. The newer work reflected his 
experiences and impressions during a period when his wife, who was suffering from a lung complaint, resided at Dr. Friedrich 
Jessen\'s Waldsanatorium in Davos, Switzerland for several months. In May and June 1912, Mann visited her and became acquainted 
with the team of doctors and patients in this cosmopolitan institution. According to Mann, in the afterword that was later 
included in the English translation of his novel, this stay inspired his opening chapter ("Arrival"). The outbreak of World War 
I interrupted his work on the book. The savage conflict and its aftermath led the author to undertake a major re-examination of 
European bourgeois society. He explored the sources of the destructiveness displayed by much of civilised humanity. He was also 
drawn to speculate about more general questions related to personal attitudes to life, health, illness, sexuality and mortality. 
His political stance also changed during this period, from anti-republican to supporting Weimar republic.[1] Given this, Mann 
felt compelled to radically revise and expand the pre-war text before completing it in 1924. Der Zauberberg was eventually 
published in two volumes by S. Fischer Verlag in Berlin. Mann\'s vast composition is erudite, subtle, ambitious, but, most of 
all, ambiguous; since its original publication it has been subject to a variety of critical assessments. For example, the book 
blends a scrupulous realism with deeper symbolic undertones. Given this complexity, each reader is obliged to interpret the 
significance of the pattern of events in the narrative, a task made more difficult by the author\'s irony. Mann was well aware 
of his book\'s elusiveness, but offered few clues about approaches to the text. He later compared it to a symphonic work 
orchestrated with a number of themes. In a playful commentary on the problems of interpretation—"The Making of The Magic 
Mountain," written 25 years after the novel\'s original publication—he recommended that those who wished to understand it should 
read it twice.', 'TheMagicMountain.jpg'),
('A Portrait of the Artist as a Young Man', 'James Joyce', 1916, 'Modernism', 'A Portrait of the Artist as a Young Man is the first 
novel of Irish writer James Joyce. A Künstlerroman written in a modernist style, it traces the religious and intellectual 
awakening of young Stephen Dedalus, Joyce\'s fictional alter ego, whose surname alludes to Daedalus, Greek mythology\'s 
consummate craftsman. Stephen questions and rebels against the Catholic and Irish conventions under which he has grown, 
culminating in his self-exile from Ireland to Europe. The work uses techniques that Joyce developed more fully in Ulysses 
(1922) and Finnegans Wake (1939). A Portrait began life in 1904 as Stephen Hero—a projected 63-chapter autobiographical novel 
in a realistic style. After 25 chapters, Joyce abandoned Stephen Hero in 1907 and set to reworking its themes and protagonist 
into a condensed five-chapter novel, dispensing with strict realism and making extensive use of free indirect speech that allows 
the reader to peer into Stephen\'s developing consciousness. American modernist poet Ezra Pound had the novel serialised in the 
English literary magazine The Egoist in 1914 and 1915, and published as a book in 1916 by B. W. Huebsch of New York. The 
publication of A Portrait and the short story collection Dubliners (1914) earned Joyce a place at the forefront of literary 
modernism.', 'APortraitOfTheArtistAsAYoungMan.jpg'),
('Midnights Children', 'Salman Rushdie', 1916, 'Modernism', 'Midnights Children is a 1981 novel by author Salman Rushdie. It 
portrays India\'s transition from British colonial rule to independence and the partition of India. It is considered an example 
of postcolonial, postmodern, and magical realist literature. The story is told by its chief protagonist, Saleem Sinai, and is 
set in the context of actual historical events. The style of preserving history with fictional accounts is self-reflexive. 
Midnight\'s Children won both the Booker Prize and the James Tait Black Memorial Prize in 1981. It was awarded the "Booker of 
Bookers" Prize and the best all-time prize winners in 1993 and 2008 to celebrate the Booker Prize 25th and 40th anniversary. In 
2003, the novel was listed on the BBC\'s The Big Read poll of the UK\'s "best-loved novels". It was also added to the list of 
Great Books of the 20th Century, published by Penguin Books.', 'MidnightsChildren.jpg');

INSERT INTO users (username, password, email, isBanned, role)
VALUES ('Admin', '$2b$10$io9NINYCsUjLmwHeAk0Co.I09.Ziv0RzyYNp848LO7Oy5kXWp3I5G', 'admin1@gmail.com', '0', '1'),
       ('marto', '$10$KM0tgPIT9JfkP8tnkiVKqO3Ng1s0BMWkkQfZIK9A99eWTTILfzeNq', 'marto@abv.bg', '0', '0'),
       ('kon4e2', '$2b$10$jUJf10a7HCXnvSqcBfGYrueCAV8hFWs/hhOnWAIevF0nim2Hdfi0W', 'kon4e2@gmail.com', '0', '0'),
       ('pesho2', '$2b$10$Vui2f4cGBRxHugI5Ap1A5.uwJI/CmsddJ1B76nkKFcXdIDb2Yh9ee', 'pesho2@gmail.com', '0', '0');

INSERT INTO reviews (content, isDeleted, books_id, users_id)
VALUES ('Very nice book. The main plot is very interesting and the characters are so amazing. Recommend it!', '0', '1', '1'),
       ('Well done and approachable for young people.', '0', '1', '3'),
       ('The ending is abrupt and left me wondering what was really going on which I suppose was the point of the ending.', '0', '1', '4'),
       ('Stunning story. Couldn’t put it down from start to finish. Reminded me of Stephen King’s Misery', '0', '2', '2'),
       ('I love this book and the interactions between the characters so I do not give any spoilers away', '0', '2', '3'),
       ('It was all filler! I swear this series just gets worse the more you read.','0', '6', '4'),
       ('A page turner filled with tension, vivid scenes and passion.', '0', '6', '1'),
       ('Really enjoyed this one. The audio is performed by the author, who is a very good reader. Heavy on the associations with 
       colonialism in human history without losing any of the entertainment value.', '0', '3', '4'),
       ('A very well written book with a message!', '0', '4', '2'),
       ('It was a very short story with not a great deal of substance. You did not get to fully interact with Hamlet.', '0', '8', '3');

INSERT INTO ratings (rating, isDeleted, books_id, users_id)
VALUES ('1', '0', '1', '1'), ('5', '0', '1', '2'), ('3', '0', '1', '3'),
       ('5', '0', '2', '2'), ('3', '0', '2', '4'),
       ('5', '0', '4', '1'),
       ('2', '0', '5', '1'), ('5', '0', '5', '4'),
       ('5', '0', '6', '1'), ('2', '0', '6', '4'),
       ('3', '0', '8', '3')


