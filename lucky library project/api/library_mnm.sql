-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema library_mnm
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema library_mnm
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `library_mnm` DEFAULT CHARACTER SET latin1 ;
USE `library_mnm` ;

-- -----------------------------------------------------
-- Table `library_mnm`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `library_mnm`.`users` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(45) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `isBanned` TINYINT(3) NULL DEFAULT NULL,
  `avatarUrl` VARCHAR(45),
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `library_mnm`.`books`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `library_mnm`.`books` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NOT NULL,
  `author` VARCHAR(100) NOT NULL,
  `year` INT(11) NOT NULL,
  `genre` VARCHAR(45) NOT NULL,
  `isBorrowed` TINYINT(3) NULL DEFAULT '0',
  `users_id` INT NULL DEFAULT NULL,
  `description` TEXT NOT NULL,
  `coverUrl` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_books_users1_idx` (`users_id` ASC) VISIBLE,
  CONSTRAINT `fk_books_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `library_mnm`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `library_mnm`.`reviews`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `library_mnm`.`reviews` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `content` VARCHAR(255) NOT NULL,
  `isDeleted` TINYINT(3) NULL DEFAULT 0,
  `books_id` INT NOT NULL,
  `users_id` INT NOT NULL,
  PRIMARY KEY (`id`, `books_id`, `users_id`),
  INDEX `fk_reviews_books_idx` (`books_id` ASC) VISIBLE,
  INDEX `fk_reviews_users1_idx` (`users_id` ASC) VISIBLE,
  CONSTRAINT `fk_reviews_books`
    FOREIGN KEY (`books_id`)
    REFERENCES `library_mnm`.`books` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_reviews_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `library_mnm`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `library_mnm`.`likes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `library_mnm`.`likes` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `like` ENUM('LIKE', 'LOVE', 'SAD', 'WOW', 'HEART') NOT NULL,
  `isDeleted` TINYINT(3) NULL DEFAULT 0,
  `reviews_id` INT(11) NOT NULL,
  `users_id` INT NOT NULL,
  PRIMARY KEY (`id`, `reviews_id`, `users_id`),
  INDEX `fk_likes_reviews1_idx` (`reviews_id` ASC) VISIBLE,
  INDEX `fk_likes_users1_idx` (`users_id` ASC) VISIBLE,
  CONSTRAINT `fk_likes_reviews1`
    FOREIGN KEY (`reviews_id`)
    REFERENCES `library_mnm`.`reviews` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_likes_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `library_mnm`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `library_mnm`.`ratings`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `library_mnm`.`ratings` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `rating` ENUM('1', '2', '3', '4', '5') NOT NULL,
  `isDeleted` TINYINT(3) NULL DEFAULT 0,
  `books_id` INT NOT NULL,
  `users_id` INT NOT NULL,
  PRIMARY KEY (`id`, `books_id`, `users_id`),
  INDEX `fk_ratings_books1_idx` (`books_id` ASC) VISIBLE,
  INDEX `fk_ratings_users1_idx` (`users_id` ASC) VISIBLE,
  CONSTRAINT `fk_ratings_books1`
    FOREIGN KEY (`books_id`)
    REFERENCES `library_mnm`.`books` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ratings_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `library_mnm`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `library_mnm`.`readbooksbyusers`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `library_mnm`.`readbooksbyusers` (
  `books_id` INT NOT NULL,
  `users_id` INT NOT NULL,
  `readOn` DATETIME NULL DEFAULT CURRENT_TIMESTAMP(),
  PRIMARY KEY (`books_id`, `users_id`),
  INDEX `fk_readbooksbyusers_users1_idx` (`users_id` ASC) VISIBLE,
  CONSTRAINT `fk_readbooksbyusers_books1`
    FOREIGN KEY (`books_id`)
    REFERENCES `library_mnm`.`books` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_readbooksbyusers_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `library_mnm`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
ALTER TABLE `library_mnm`.`users` 
ADD COLUMN `role` ENUM('0', '1') NOT NULL DEFAULT '0' AFTER `isBanned`;
ALTER TABLE `library_mnm`.`books` 
ADD COLUMN `isDeleted` TINYINT(3) NULL DEFAULT '0' AFTER `users_id`;

CREATE TABLE `library_mnm`.`tokens` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `token` VARCHAR(255) NULL,
  PRIMARY KEY (`id`));
ALTER TABLE `library_mnm`.`likes` 
CHANGE COLUMN `like` `typelike` ENUM('LIKE', 'LOVE', 'HEARTH', 'SAD', 'WOW') NULL DEFAULT NULL ;


