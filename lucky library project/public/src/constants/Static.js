const martins = [
    {
      title: 'Martin Yanev',
      description:
        "23 year old duck tycoon",
      imageUrl: process.env.PUBLIC_URL + '/assets/yanev.jpg',
      time: 1500,
    },
    {
      title: 'Martin Jordanov',
      description:
        '21 year old Bob the builder',
      imageUrl: process.env.PUBLIC_URL + '/assets/jordanov.jpg',
      time: 1500,
    },
  ];

  export default martins