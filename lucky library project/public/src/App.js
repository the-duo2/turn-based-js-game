import { BrowserRouter, Redirect, Route, Switch } from "react-router-dom";
import { useState } from "react";
import Home from "./components/Home/Home";
import Books from "./components/Books/Books";
import Trending from "./components/Trending/Trending";
import { CssBaseline } from "@material-ui/core";
import Header from "./components/Header/Header";
import AuthContext, { getUser } from "./providers/authContext";
import ProfilePage from "./components/ProfilePage/Profile";

function App() {
  const [authValue, setAuthValue] = useState({
    user: getUser(),
    isLoggedIn: Boolean(getUser()),
  });

  return (
    <div>
      <CssBaseline />
      <BrowserRouter>
        <AuthContext.Provider
          value={{ ...authValue, setAuthState: setAuthValue }}
        >
          <Header />
          <Switch>
            <Redirect path="/" exact to="/home" />
            <Route path="/home" component={Home} />
            <Route path="/books" component={Books} />
            <Route path="/trending" component={Trending} />
            <Route path="/profile" component={ProfilePage} />
          </Switch>
        </AuthContext.Provider>
      </BrowserRouter>
    </div>
  );
}

export default App;
