import { createContext, useContext } from "react";
import jwtDecode from "jwt-decode";

const AuthContext = createContext({
  isLoggedIn: false,
  user: null,
  setAuthState: () => {},
});

export const useAuth = () => {
  useContext(AuthContext);
};

export const getToken = () => {
  return localStorage.getItem("tokenFinal");
};

export const getUser = () => {
  try {
    return jwtDecode(localStorage.getItem("tokenFinal") || "");
  } catch (error) {
    return null;
  }
};

export default AuthContext;
