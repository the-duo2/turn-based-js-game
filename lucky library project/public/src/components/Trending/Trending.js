import React, { useState, useEffect } from "react";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import { BASE_URL } from "../../constants/constants";
import { makeStyles, Typography } from "@material-ui/core";
import { useCoverCardMediaStyles } from "@mui-treasury/styles/cardMedia/cover";
import SingleBook from "../Books/SingleBook";

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover",
    backgroundColor: "black",
    opacity: "1",
    backgroundImage: `linear-gradient(to bottom, rgba(255, 146, 0, 1), rgba(0, 0, 0, 0.1)),
    url(${process.env.PUBLIC_URL + "/assets/blackwhite.jpg"})`,
    filter: "brightness(90%)",
  },
  slider: {
    position: "relative",
    height: "86vh",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  image: {
    width: "600px",
    height: "600px",
    borderRadius: "10px",
  },
  rightArrow: {
    position: "absolute",
    top: "50%",
    right: "70px",
    fontSize: "5rem",
    zIndex: "10",
    cursor: "pointer",
    userSelect: "none",
    color: "#fff",
  },
  leftArrow: {
    position: "absolute",
    top: "50%",
    left: "70px",
    fontSize: "5rem",
    zIndex: "10",
    cursor: "pointer",
    userSelect: "none",
    color: "#fff",
  },
  slide: {
    opacity: 0,
    transitionDuration: "1s ease",
  },
  slideActive: {
    opacity: 1,
    transitionDuration: "1s",
    transform: "scale(1.08)",
  },
  card: {
    borderRadius: "1rem",
    boxShadow: "none",
    position: "relative",
    width: 450,
    height: 600,
    "&:after": {
      content: '""',
      display: "block",
      position: "absolute",
      width: "100%",
      height: "64%",
      bottom: 0,
      zIndex: 1,
      background: "linear-gradient(to top, #000, rgba(0,0,0,0))",
    },
  },
  content: {
    position: "absolute",
    zIndex: 2,
    bottom: 0,
    width: "100%",
  },
  titles: {
    fontFamily: "Orbitron",
    color: "#fff",
    fontSize: "4.5rem",
    fontWeight: "900",
    display: "grid",
    justifyContent: "center",
  },
}));

const Trending = ({ slides }) => {
  const [checked, setChecked] = useState(false);

  useEffect(() => {
    setChecked(true);
  }, []);

  const mediaStyles = useCoverCardMediaStyles({ bgPosition: "top" });
  const classes = useStyles();

  const [books, setBooks] = useState([]);
  const [isLoading, setIsLoading] = useState(null);
  const [error, setError] = useState(null);

  const [currentTop, setCurrentTop] = useState(4);
  const [currentNew, setCurrentNew] = useState(5);
  const length = books.length;

  useEffect(() => {
    setIsLoading(true);

    fetch(`${BASE_URL}/books`)
      .then((res) => res.json())
      .then((data) => {
        if (!Array.isArray(data)) {
          throw new Error("error");
        }
        setBooks(data);
      })
      .catch((error) => setError(error))
      .finally(() => setIsLoading(false));
  }, []);

  const nextSlideTop = () => {
    setCurrentTop(currentTop === length - 1 ? 0 : currentTop + 1);
  };

  const prevSlideTop = () => {
    setCurrentTop(currentTop === 0 ? length - 1 : currentTop - 1);
  };
  const nextSlideNew = () => {
    setCurrentNew(currentNew === length - 1 ? 0 : currentNew + 1);
  };

  const prevSlideNew = () => {
    setCurrentNew(currentNew === 0 ? length - 1 : currentNew - 1);
  };

  return (
    <div className={classes.root}>
      <div className={classes.topPicks}>
        <Typography className={classes.titles}>Top picks</Typography>
        <section className={classes.slider}>
          <ChevronLeftIcon
            className={classes.leftArrow}
            onClick={prevSlideTop}
          />
          <ChevronRightIcon
            className={classes.rightArrow}
            onClick={nextSlideTop}
          />
          {books.map((book, index) => {
            return (
              <div
                className={
                  index === currentTop ? classes.slideActive : classes.slide
                }
                key={index}
              >
                {index === currentTop && (
                  <SingleBook
                    key={book.id}
                    book={book}
                    mediaStyles={mediaStyles}
                    styles={classes}
                  />
                )}
              </div>
            );
          })}
        </section>
      </div>
      <div className={classes.new}>
        <Typography className={classes.titles}>New </Typography>
        <section className={classes.slider}>
          <ChevronLeftIcon
            className={classes.leftArrow}
            onClick={prevSlideNew}
          />
          <ChevronRightIcon
            className={classes.rightArrow}
            onClick={nextSlideNew}
          />
          {books.map((book, index) => {
            return (
              <div
                className={
                  index === currentNew ? classes.slideActive : classes.slide
                }
                key={index}
              >
                {index === currentNew && (
                  <SingleBook
                    key={book.id}
                    book={book}
                    mediaStyles={mediaStyles}
                    styles={classes}
                  />
                )}
              </div>
            );
          })}
        </section>
      </div>
    </div>
  );
};

export default Trending;
