import React from "react";
import Dialog from "@material-ui/core/Dialog";
import LoginForm from "./LoginForm";

function SimpleDialog(props) {
  const { onClose, selectedValue, open } = props;

  const handleClose = () => {
    onClose(selectedValue);
  };

  return (
    <Dialog
      onClose={handleClose}
      aria-labelledby="simple-dialog-title"
      open={open}
    >
      <LoginForm />
    </Dialog>
  );
}

export default SimpleDialog;
