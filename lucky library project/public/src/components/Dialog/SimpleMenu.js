import React from "react";
import IconButton from "@material-ui/core/Button";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import SortIcon from "@material-ui/icons/Sort";
import { makeStyles } from "@material-ui/core/styles";
import { Link } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  appbar: {
    background: "grey",
    fontFamily: "Orbitron",
  },
  appbarTitle: {
    flexGrow: "1",
  },
  appbarWrapper: {
    width: "100%",
    margin: "0 auto",
  },
  button: {
    fontFamily: "Orbitron",
    fontSize: "3rem",
    color: "#fff",
    borderStyle: "none",
    fontWeight: "900",
    display: "grid",
  },
}));

export default function SimpleMenu() {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <div>
      <IconButton
        aria-controls="simple-menu"
        aria-haspopup="true"
        onClick={handleClick}
      >
        <SortIcon className={classes.button} />
      </IconButton>
      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <MenuItem component={Link} to="/Home">
          Home
        </MenuItem>
        <MenuItem component={Link} to="/books">
          Books
        </MenuItem>
        <MenuItem component={Link} to="/trending">
          Trending
        </MenuItem>
      </Menu>
    </div>
  );
}
