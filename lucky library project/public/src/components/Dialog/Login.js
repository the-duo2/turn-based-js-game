import {
  Grid,
  Paper,
  Button,
  Typography,
  ButtonGroup,
} from "@material-ui/core";
import { TextField } from "@material-ui/core";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import { BASE_URL } from "../../constants/constants";

import decode from 'jwt-decode';
import AuthContext from '../../providers/authContext';
import { useState, useContext } from 'react';
import PopUpSingleBookAlert from "../Books/PopUpSingleBookAlert";


const Login = ({ registerToggle }) => {
  const paperStyle = { padding: "20px 20px 20px 20px", width: 250 };
  const btnStyle = {
    marginTop: 10,
    backgroundColor: "orange",
    fontFamily: "Orbitron",
  };
  const passwordRegExp = /^(?=.*\d)(?=.*[a-z]).{5,}$/;

  const auth = useContext(AuthContext);

  const [user, setUser] = useState({
    username: "",
    password: "",
    email: "",
  });

  const [notify, setNotify] = useState({
    isOpen: false,
    message: '',
    type: ''
  });

  const validationSchemaLogin = Yup.object().shape({
    name: Yup.string().min(3, "It's too short").required("Required"),
    password: Yup.string()
      .min(5, "Minimum characters should be 5")
      .matches(
        passwordRegExp,
        "Password must have at least 5 characters and a number"
      )
      .required("Required"),
  });

  const onLogin = (values) => {
    fetch(`${BASE_URL}/users/login`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        username: values.name,
        password: values.password,
      }),
    })
    .then(r => r.json())
    .then(({ tokenFinal }) => {
        if (!tokenFinal) {
          setNotify({
            isOpen: true,
            message: 'invalid password or username',
            type: 'error'
          });
          return;
        }
        const user = decode(tokenFinal);
        setUser(user)
        localStorage.setItem('tokenFinal', tokenFinal);
        auth.setAuthState({ user, isLoggedIn: true });
        window.location.reload()
    })
    .catch(console.warn);
  };

  const handleCloseNotification = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    setNotify({
      isOpen: false
    });
  };

  return (
    <Grid>
      <PopUpSingleBookAlert notify={notify} handleCloseNotification={handleCloseNotification}/>
      <Paper elevation={0} style={paperStyle}>
        <Grid align="center">
          <Typography variant="caption">Fill the form to login</Typography>
        </Grid>
        <Formik
          initialValues={user}
          validationSchema={validationSchemaLogin}
          onSubmit={onLogin}
        >
          {(props) => (
            <Form noValidate autocomplete="off">
              <Field
                as={TextField}
                name="name"
                label="Username"
                fullWidth
                error={props.errors.name && props.touched.name}
                helperText={<ErrorMessage name="name" />}
                required
              />

              <Field
                as={TextField}
                name="password"
                label="Password"
                type="password"
                fullWidth
                error={props.errors.password && props.touched.password}
                helperText={<ErrorMessage name="password" />}
                required
              />

              <ButtonGroup disableElevation variant="outlined" color="primary">
                <Button
                  type="submit"
                  style={btnStyle}
                  variant="contained"
                  color="default"
                >
                  Login
                </Button>
                <Button
                  type="button"
                  style={btnStyle}
                  variant="contained"
                  onClick={() => registerToggle()}
                  color="link"
                >
                  Register
                </Button>
              </ButtonGroup>
            </Form>
          )}
        </Formik>
      </Paper>
    </Grid>
  );
};

export default Login;
