import React from "react";
import Dialog from "@material-ui/core/Dialog";
import CreateBook from "../Books/CreateBook";

function CreateBookDialog(props) {
  const { onClose, selectedValue, open } = props;

  const handleClose = () => {
    onClose(selectedValue);
  };

  return (
    <Dialog
      onClose={handleClose}
      aria-labelledby="simple-dialog-title"
      open={open}
    >
      <CreateBook />
    </Dialog>
  );
}

export default CreateBookDialog;
