import React from "react";
import { useState, useContext } from "react";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import { makeStyles } from "@material-ui/core/styles";
import { Link } from "react-router-dom";
import AccountBoxIcon from "@material-ui/icons/AccountBox";
import { IconButton } from "@material-ui/core";
import AuthContext from "../../providers/authContext";

const useStyles = makeStyles((theme) => ({
  button: {
    fontFamily: "Orbitron",
    fontSize: "2.7rem",
    color: "#fff",
    borderStyle: "none",
    fontWeight: "900",
    display: "grid",
  },
}));

export default function AccountMenu() {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = useState(null);
  const auth = useContext(AuthContext);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const logout = () => {
    localStorage.clear();
    auth.setAuthState({
      user: null,
      isLoggedIn: false,
    });
    setTimeout(() => {
      window.location.reload();
    }, 10);
  };

  return (
    <div>
      <IconButton
        aria-controls="simple-menu"
        aria-haspopup="true"
        onClick={handleClick}
      >
        <AccountBoxIcon className={classes.button} />
      </IconButton>
      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <MenuItem component={Link} to="/profile">
          Profile
        </MenuItem>
        <MenuItem onClick={logout} component={Link} to="/home">
          Logout
        </MenuItem>
      </Menu>
    </div>
  );
}
