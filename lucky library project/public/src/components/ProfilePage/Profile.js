import { makeStyles } from "@material-ui/core";
import { useState, useEffect } from "react";
import Footer from "../Footer/Footer";
import NormalUser from "./NormalUser";

const useStyles = makeStyles({
  mainRoot: {
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover",
    backgroundColor: "black",
    opacity: "1",
    backgroundImage: `linear-gradient(to bottom, rgba(255, 146, 0, 0.1), rgba(0, 0, 0, 1)),
      url(${process.env.PUBLIC_URL + "/assets/blackwhite.jpg"})`,
  },
  rootLanding: {
    display: "grid",
    justifyContent: "center",
    alignItems: "center",
    height: "100vh",
    fontFamily: "Nunito",
  },
  appbar: {
    background: "none",
  },
  appbarWrapper: {
    width: "80%",
    margin: "0 auto",
  },
  appbarTitle: {
    flexGrow: "1",
  },
  icon: {
    color: "#fff",
    fontSize: "2rem",
  },
  colorText: {
    color: "orange",
  },
  container: {
    textAlign: "center",
  },
  title: {
    fontFamily: "Orbitron",
    color: "#fff",
    fontSize: "4.5rem",
    fontWeight: "900",
  },
  goDown: {
    color: "#fff",
    fontSize: "4rem",
  },
});

const ProfilePage = () => {
  const classes = useStyles();
  const [checked, setChecked] = useState(false);

  useEffect(() => {
    setChecked(true);
  }, []);

  return (
    <div>
      <div className={classes.mainRoot} id="home">
        <NormalUser />
        <Footer />
      </div>
    </div>
  );
};

export default ProfilePage;
