import { Button, Input } from "@material-ui/core";
import { useState } from "react";
import { getToken } from "../../providers/authContext";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "inline",
    width: "50%",
  },
  input: {
    display: "none",
  },
  buttons: {
    marginTop: -10,
    width: "50%",
    fontFamily: "Orbitron",
    fontSize: "2rem",
    fontWeight: "900",
    color: "orange",
  },
}));

const UploadProfilePicture = () => {
  const [file, setFile] = useState(null);
  const classes = useStyles();

  const uploadFile = async (e) => {
    e.preventDefault();
    const fileData = new FormData();
    fileData.set("avatar", file);
    console.log(file[0]);

    const response = await fetch(`http://localhost:5555/users/avatar`, {
      method: "PUT",
      headers: {
        Authorization: `Bearer ${getToken()}`,
      },
      body: fileData,
    });

    const data = await response.json();
    console.log(data);
    window.location.reload();
  };
  return (
    <div>
      <form onSubmit={uploadFile} className={classes.root}>
        <Button className={classes.buttons}>
          <label>
            Upload
            <Input
              type="file"
              onChange={(e) => setFile(e.target.files[0])}
              className={classes.input}
            />
          </label>
        </Button>
        <Button
          type="submit"
          className={classes.buttons}
          onClick={() => console.log(file)}
        >
          SUBMIT
        </Button>
      </form>
    </div>
  );
};

export default UploadProfilePicture;
