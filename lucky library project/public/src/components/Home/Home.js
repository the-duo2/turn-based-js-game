import { makeStyles } from "@material-ui/core";
import { useState, useEffect } from "react";
import { IconButton, Collapse } from "@material-ui/core";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import AboutUs from "../AboutUs/AboutUs";
import { Link as Scroll } from "react-scroll";
import Footer from "../Footer/Footer";

const useStyles = makeStyles({
  mainRoot: {
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover",
    backgroundColor: "black",
    opacity: "1",
    backgroundImage: `linear-gradient(to bottom, rgba(255, 146, 0, 0.1), rgba(0, 0, 0, 1)),
    url(${process.env.PUBLIC_URL + "/assets/blackwhite.jpg"})`,
  },
  rootLanding: {
    display: "grid",
    justifyContent: "center",
    alignItems: "center",
    height: "100vh",
    fontFamily: "Nunito",
  },
  appbar: {
    background: "none",
  },
  appbarWrapper: {
    width: "80%",
    margin: "0 auto",
  },
  appbarTitle: {
    flexGrow: "1",
  },
  icon: {
    color: "#fff",
    fontSize: "2rem",
  },
  colorText: {
    color: "orange",
  },
  container: {
    textAlign: "center",
  },
  title: {
    fontFamily: "Orbitron",
    color: "#fff",
    fontSize: "4.5rem",
    fontWeight: "900",
  },
  goDown: {
    color: "#fff",
    fontSize: "4rem",
  },
});

const Home = () => {
  const classes = useStyles();
  const [checked, setChecked] = useState(false);

  useEffect(() => {
    setChecked(true);
  }, []);

  return (
    <div>
      <div className={classes.mainRoot} id="home">
        <div className={classes.rootLanding}>
          <Collapse
            in={checked}
            {...(checked ? { timeout: 1000 } : {})}
            collapsedHeight={50}
          >
            <div className={classes.container}>
              <h1 className={classes.title}>
                Welcome to <br />
                Our <span className={classes.colorText}>Library.</span>
              </h1>
              <Scroll to="place-to-visit" smooth={true}>
                <IconButton>
                  <ExpandMoreIcon className={classes.goDown} />
                </IconButton>
              </Scroll>
            </div>
          </Collapse>
        </div>
        <AboutUs />
        <Footer />
      </div>
    </div>
  );
};

export default Home;
