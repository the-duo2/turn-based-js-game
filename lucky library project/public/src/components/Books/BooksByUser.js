import { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { useCoverCardMediaStyles } from "@mui-treasury/styles/cardMedia/cover";
import { BASE_URL } from "../../constants/constants";
import { Grid, Typography } from "@material-ui/core";
import SingleBook from "./SingleBook";
import { getToken } from "../../providers/authContext";
import { Button } from "react-scroll";
import jwtDecode from 'jwt-decode'


const useStyles = makeStyles((theme) => ({
  card: {
    borderRadius: "1rem",
    boxShadow: "none",
    position: "relative",
    margin: 30,
    width: 350,
    height: 500,
    "&:after": {
      content: '""',
      display: "block",
      position: "absolute",
      width: "100%",
      height: "64%",
      bottom: 0,
      zIndex: 1,
      background: "linear-gradient(to top, #000, rgba(0,0,0,0))",
    },
  },
  content: {
    position: "absolute",
    zIndex: 2,
    bottom: 0,
    width: "100%",
  },
  root: {
    flexGrow: 1,
    margin: "0 auto",
    alignItems: "center",
    width: 1500,
  },
  page: {
    marginBottom: "2%",
    float: "right",
  },
  whole: {
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover",
    backgroundColor: "transparent",
    filter: "brightness(90%)",
    opacity: "1",
  },
  h1style: {
    display: "grid",
    textAlign: "center",
    marginTop: "0",
    fontSize: "3rem",
    fontFamily: "Orbitron",
    color: "orange",
    fontWeight: "900",
    paddingTop: "2%",
    height: "30vh",
    alignItems: "center",
    justifyContent: "center",
  },
  btn: {
    color: "#fff",
    fontWeight: "700",
    fontFamily: "Orbitron",
  },
}));

const BooksbyUsers = () => {
  const mediaStyles = useCoverCardMediaStyles({ bgPosition: "top" });
  const styles = useStyles();

  const [books, setBooks] = useState([]);
  const [isLoading, setIsLoading] = useState(null);
  const [error, setError] = useState(null);

  
  const user = jwtDecode(localStorage.getItem("tokenFinal"))
  const [page, setPage] = useState("0");
  
  useEffect(() => {
    setIsLoading(true);

    fetch(`${BASE_URL}/books/books/${user.id}`, {
      headers: {
        Authorization: `Bearer ${getToken()}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        if (!Array.isArray(data)) {
          throw new Error("error");
        }
        setBooks(data);
      })
      .catch((error) => setError(error))
      .finally(() => setIsLoading(false));
  }, [page]);

  return (
    <div className={styles.whole}>
      <div className={styles.root}>
        
        {books.length > 0 ? (
          <Typography className={styles.h1style}>Currently Reading</Typography>
        ) : (
          ""
        )}
        {books.length > 0 ? (
          <Grid container spacing={7}>
            {books.map((book) => (
              <SingleBook
                key={book.id}
                book={book}
                mediaStyles={mediaStyles}
                styles={styles}
              />
            ))}
          </Grid>
        ) : (
          <Typography className={styles.h1style}>
            No books currently borrowed!
          </Typography>
        )}
      </div>
    </div>
  );
};

export default BooksbyUsers;
