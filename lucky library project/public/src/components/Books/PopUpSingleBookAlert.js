import Snackbar from "@material-ui/core/Snackbar";
import Alert from "@material-ui/lab/Alert";

const PopUpSingleBookAlert = ({ notify, handleCloseNotification }) => {
  return (
    <>
      <Snackbar
        anchorOrigin={{ vertical: "top", horizontal: "center" }}
        open={notify.isOpen}
        autoHideDuration={6000}
        onClose={handleCloseNotification}
      >
        <Alert
          onClose={handleCloseNotification}
          severity={notify.type}
          variant="filled"
        >
          {notify.message}
        </Alert>
      </Snackbar>
    </>
  );
};

export default PopUpSingleBookAlert;
