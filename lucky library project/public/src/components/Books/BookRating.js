import Rating from "@material-ui/lab/Rating";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import { useEffect, useState } from "react";
import { BASE_URL } from "../../constants/constants";

const BookRating = ({ book, refreshReviews }) => {
  const [value, setValue] = useState(2);

  useEffect(() => {
    fetch(`${BASE_URL}/books/${book.id}/rate`)
      .then((res) => res.json())
      .then((data) => {
        if (data.length === 0) {
          setValue("");
        } else {
          const values = data.map((el) => Number(el.rating));
          const avg =
            values.reduce((acc, curr) => (acc += curr), 0) / values.length;
          setValue(avg);
        }
      });
  }, [refreshReviews, book.id]);

  return (
    <div>
      <Box component="fieldset" mb={3} borderColor="transparent">
        {value === "" ? (
          <Typography>No rating yet</Typography>
        ) : (
          <Rating name="read-only" value={value} readOnly />
        )}
      </Box>
    </div>
  );
};

export default BookRating;
