import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import DialogTitle from "@material-ui/core/DialogTitle";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import { BASE_URL } from "../../constants/constants";
import PopUpSingleBookAlert from "./PopUpSingleBookAlert";
import { useState } from "react";
import CreateReview from "./CreateReview";
import ReviewsByBook from "./ReviewsByBook";
import BookRating from "./BookRating";
import RateBook from "./RateBook";

const useStyles = makeStyles((theme) => ({
  dialog: {
    fontFamily: "Orbitron",
    fontWeight: "900",
  },
  img: {
    float: "left",
    paddingRight: "3%",
  },
  button: {
    fontSize: "1rem",
    backgroundColor: "rgba(0,0,0,0.4)",
    marginTop: 10,
    width: "700px",
    fontFamily: "Orbitron",
    fontWeight: "900",
    color: "orange",
  },
  title: {
    fontFamily: "Orbitron",
    fontWeight: "900",
  },
  text: {
    fontFamily: "Orbitron",
    fontWeight: "1000",
    fontSize: "2.5rem",
  },
  descriptions: {
    fontFamily: "Orbitron",
    fontWeight: "900",
    color: "orange",
    display: "inline",
  },
}));

const PopUpSingleBook = ({ open, handleClose, book, imageUrl }) => {
  const styles = useStyles();
  const [refreshReviews, setRefreshReviews] = useState(false);
  const [notify, setNotify] = useState({
    isOpen: false,
    message: "",
    type: "",
  });

  const handleCloseNotification = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setNotify({
      isOpen: false,
    });
  };

  const borrowBook = () => {
    const token = localStorage.getItem("tokenFinal");

    if (!token) {
      setNotify({
        isOpen: true,
        message: "You have to log in before borrowing book",
        type: "error",
      });
      return;
    }

    fetch(`${BASE_URL}/books/${+book.id}`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        if (data.error) {
          setNotify({
            isOpen: true,
            message: data.error,
            type: "error",
          });
          return;
        }
        setNotify({
          isOpen: true,
          message: `Successfully borrowed ${book.name}`,
          type: "success",
        });
      });
  };

  const returnBook = () => {
    const token = localStorage.getItem("tokenFinal");

    if (!token) {
      setNotify({
        isOpen: true,
        message: "You have to log in before returning book",
        type: "error",
      });
    }

    fetch(`${BASE_URL}/books/${+book.id}`, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        if (data.error) {
          setNotify({
            isOpen: true,
            message: data.error,
            type: "error",
          });
          return;
        }
        setNotify({
          isOpen: true,
          message: `Successfully returned ${book.name}`,
          type: "success",
        });
      });
  };

  return (
    <>
      <Dialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
        className={styles.dialog}
      >
        <PopUpSingleBookAlert
          notify={notify}
          handleCloseNotification={handleCloseNotification}
          className={styles.title}
        />
        <DialogTitle id="customized-dialog-title" onClose={handleClose}>
          <Typography className={styles.text}>{book.name}</Typography>
          <Typography>
            <BookRating book={book} refreshReviews={refreshReviews} />
          </Typography>
        </DialogTitle>

        <DialogContent dividers>
          <img src={imageUrl} alt="..." width="300px" className={styles.img} />

          <Typography gutterBottom className={styles.title}>
            <Typography className={styles.descriptions}>Author:</Typography>{" "}
            {book.author}{" "}
          </Typography>
          <Typography gutterBottom className={styles.title}>
            <Typography className={styles.descriptions}>Genre:</Typography>{" "}
            {book.genre}{" "}
          </Typography>
          <Typography gutterBottom className={styles.title}>
            <Typography className={styles.descriptions}>Year:</Typography>{" "}
            {book.year}{" "}
          </Typography>
          <Typography gutterBottom>
            <Typography className={styles.descriptions}>
              Description:
            </Typography>{" "}
            {book.description}{" "}
          </Typography>
          <hr />
          <CreateReview
            book={book}
            setNotify={setNotify}
            setRefreshReviews={setRefreshReviews}
          />
          <ReviewsByBook
            book={book}
            setRefreshReviews={setRefreshReviews}
            refreshReviews={refreshReviews}
          />
          <hr />
          <RateBook
            book={book}
            setNotify={setNotify}
            setRefreshReviews={setRefreshReviews}
            refreshReviews={refreshReviews}
          />
        </DialogContent>
        <DialogActions>
          <Button
            autoFocus
            onClick={() => borrowBook()}
            color="primary"
            variant="contained"
            className={styles.button}
          >
            Borrow book
          </Button>
          <Button
            autoFocus
            onClick={() => returnBook()}
            color="secondary"
            variant="contained"
            className={styles.button}
          >
            Return book
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};

export default PopUpSingleBook;
