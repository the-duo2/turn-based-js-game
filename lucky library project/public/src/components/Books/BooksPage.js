import Button from "@material-ui/core/Button";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import { useState } from "react";

const BooksPage = ({ styles, handlePage, page, handleClose }) => {
  const [anchorEl, setAnchorEl] = useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose1 = (num) => {
    handlePage(num);
    setAnchorEl(null);
  };

  return (
    <div className={styles.page}>
      <Button
        aria-controls="simple-menu"
        aria-haspopup="true"
        onClick={handleClick}
        size={"large"}
        className={styles.btn}
        variant={"outlined"}
      >
        Page {page}
      </Button>
      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <MenuItem onClick={() => handleClose1(0)}>0</MenuItem>
        <MenuItem onClick={() => handleClose1(1)}>1</MenuItem>
        <MenuItem onClick={() => handleClose1(2)}>2</MenuItem>
        <MenuItem onClick={() => handleClose1(3)}>3</MenuItem>
      </Menu>
    </div>
  );
};

export default BooksPage;
