import Box from "@material-ui/core/Box";
import Card from "@material-ui/core/Card";
import CardMedia from "@material-ui/core/CardMedia";
import { Info, InfoSubtitle, InfoTitle } from "@mui-treasury/components/info";
import { useGalaxyInfoStyles } from "@mui-treasury/styles/info/galaxy";
import { ButtonBase, Grid } from "@material-ui/core";
import { useState } from "react";
import PopUpSingleBook from "./PopUpSingleBook";
import { BASE_URL } from "../../constants/constants";

const SingleBook = ({ book, mediaStyles, styles }) => {
  const imageUrl = `${BASE_URL}/coverUrl/${book.coverUrl}`;

  // novo
  const [open, setOpen] = useState(false);

  const handleClickOpen = () => setOpen(true);

  const handleClose = () => setOpen(false);

  return (
    <Grid item xs={12} sm={6} md={4} lg={3}>
      <PopUpSingleBook
        open={open}
        handleClose={handleClose}
        book={book}
        imageUrl={imageUrl}
      />

      <ButtonBase onClick={handleClickOpen}>
        <Card className={styles.card}>
          <CardMedia classes={mediaStyles} image={imageUrl} />
          <Box py={3} px={2} className={styles.content}>
            <Info useStyles={useGalaxyInfoStyles}>
              <InfoSubtitle>{book.author}</InfoSubtitle>
              <InfoTitle>{book.name}</InfoTitle>
            </Info>
          </Box>
        </Card>
      </ButtonBase>
    </Grid>
  );
};

export default SingleBook;
