import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import SearchIcon from "@material-ui/icons/Search";
import { useEffect, useState } from "react";
import { BASE_URL } from "../../constants/constants";
import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    justifyContent: "left",
    alignItems: "center",
    marginBottom: -70,
  },

  searchBar: {
    width: "500px",
    backgroundColor: "rgba(0,0,0,0.2)",
    transition: "0.5s",
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: "50ch",
  },
  button: {
    display: "inline-block",
    position: "relative",
    justifyContent: "center",
    alignItems: "center",
    fontSize: "1.5rem",
    fontFamily: "Orbitron",
    fontWeight: "900",
    color: "#fff",
  },
  smallButton: {
    fontSize: "3rem",
    color: "#fff",
  },
}));

const SearchBar = ({ setBooks }) => {
  const classes = useStyles();
  const [searchParam, setSearchParam] = useState("");
  const [param, setParam] = useState("name");

  useEffect(() => {
    console.log(searchParam);
    fetch(`${BASE_URL}/books?${param}=${searchParam}&pageSize=16`)
      .then((res) => res.json())
      .then((data) => {
        if (data.msg) {
          setBooks([]);
          return;
        }
        setBooks(data);
      });
  }, [searchParam]);

  const changeParam = () => {
    if (param === "name") {
      setParam("author");
    } else {
      setParam("name");
    }
  };

  return (
    <>
      <form autoComplete="off">
        <div className={classes.root}>
          <TextField
            label="Search"
            id="filled-margin-normal"
            helperText="Search by author or book name!"
            margin="normal"
            variant="filled"
            onChange={(e) => setSearchParam(e.target.value)}
            className={classes.searchBar}
          />
          <Button className={classes.smallButton}>
            <SearchIcon className={classes.smallButton} fontSize="large" />
          </Button>
          <Button
            onClick={changeParam}
            variant="outlined"
            className={classes.button}
          >
            {param}
          </Button>
        </div>
      </form>
    </>
  );
};

export default SearchBar;
