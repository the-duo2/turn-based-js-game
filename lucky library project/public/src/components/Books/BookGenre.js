import Button from "@material-ui/core/Button";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import { useState, useEffect } from 'react';
import { BASE_URL } from "../../constants/constants";

const BookGenre = ({ styles, setBooks, page }) => {
  const [anchorEl, setAnchorEl] = useState(null);
  const [genre, setGenre] = useState('');

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const filter = (genre) => {
    if (!genre) {
      fetch(`${BASE_URL}/books`)
        .then(res => res.json())
        .then(data => setBooks(data));
    }
    fetch(`${BASE_URL}/books?genre=${genre}&page=${page}`)
      .then(res => res.json())
      .then(data => setBooks(data));
  }

  const handleClose1 = (genre) => {
    filter(genre);
    setGenre(genre);
    setAnchorEl(null);
  };

  return (
    <div className={styles.page} >
      <Button
        aria-controls="simple-menu"
        aria-haspopup="true"
        onClick={handleClick}
        size={"large"}
        className={styles.btn}
        variant={"outlined"}
      >
        Genre {genre}
      </Button>
      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
      >
        <MenuItem onClick={() => handleClose1('')}>All</MenuItem>
        <MenuItem onClick={() => handleClose1('Novel')}>Novel</MenuItem>
        <MenuItem onClick={() => handleClose1('Modernism')}>Modernism</MenuItem>
        <MenuItem onClick={() => handleClose1('Poetry')}>Poetry</MenuItem>
        <MenuItem onClick={() => handleClose1('Fantasy')}>Fantasy</MenuItem>
        <MenuItem onClick={() => handleClose1('Tragedy')}>Tragedy</MenuItem>
        <MenuItem onClick={() => handleClose1('Bildungsroman')}>Bildungsroman</MenuItem>
      </Menu>
    </div>
  )
};

export default BookGenre;