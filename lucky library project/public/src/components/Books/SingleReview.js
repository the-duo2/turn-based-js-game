import { useEffect, useState } from "react";
import { BASE_URL } from "../../constants/constants";
import Button from "@material-ui/core/Button";
import DeleteIcon from "@material-ui/icons/Delete";
import { getToken, getUser } from "../../providers/authContext";

const SingleReview = ({
  review,
  book,
  setRefreshReviews,
  refreshReviews,
  setReviews,
}) => {
  const [author, setAuthor] = useState();
  const user = getUser();
  useEffect(() => {
    const token = getToken();

    fetch(`${BASE_URL}/users/${review.users_id}`, {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((data) => setAuthor(data.username));
  }, []);

  const deleteReview = () => {
    const token = getToken();

    fetch(`${BASE_URL}/books/${book.id}/reviews/${review.id}`, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data.error) {
          setReviews([]);
          return;
        }
        setRefreshReviews(!refreshReviews);
      });
  };

  return (
    <>
      <p>{review.content}</p>
      <span>author: {author}</span>
      {user && author === user.username && (
        <Button onClick={deleteReview}>
          <DeleteIcon />
        </Button>
      )}
    </>
  );
};

export default SingleReview;
