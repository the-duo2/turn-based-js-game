import {
  Grid,
  Paper,
  Button,
  Typography,
  ButtonGroup,
  Input,
} from "@material-ui/core";
import { TextField } from "@material-ui/core";
import { Formik, Form, Field } from "formik";
import { BASE_URL } from "../../constants/constants";
import { getToken } from "../../providers/authContext";
import { useState } from "react";
import PopUpSingleBookAlert from "./PopUpSingleBookAlert";

const CreateBook = () => {
  const paperStyle = { padding: "20px 20px 20px 20px", width: 250 };
  const btnStyle = {
    marginTop: 10,
    backgroundColor: "orange",
    fontFamily: "Orbitron",
  };

  const [file, setFile] = useState(null);
  const [image, setImage] = useState("");
  const [notify, setNotify] = useState({
    isOpen: false,
    message: "",
    type: "",
  });

  const handleCloseNotification = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setNotify({
      isOpen: false,
    });
  };

  const [book, setBook] = useState({
    name: "",
    author: "",
    year: "",
    genre: "",
    description: "",
    coverUrl: "",
  });

  const onCreate = (values) => {
    if (!image) {
      setNotify({
        isOpen: true,
        message: "No image selected!",
        type: "error",
      });
      return;
    }

    fetch(`${BASE_URL}/books`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${getToken()}`,
      },
      body: JSON.stringify({
        name: values.name,
        author: values.author,
        year: +values.year,
        genre: values.genre,
        description: values.description,
        coverUrl: image,
      }),
    })
      .then((r) => r.json())
      .then((r) => {
        try {
          setBook({ ...r });
          setNotify({
            isOpen: true,
            message: "Successfully created book!",
            type: "success",
          });
        } catch (error) {
          setNotify({
            isOpen: true,
            message: error,
            type: "error",
          });
        }
      })
      .catch(console.warn);
  };

  const onUpload = () => {
    const fileData = new FormData();
    fileData.set("coverUrl", file);

    fetch(`http://localhost:5555/books/coverUrl`, {
      method: "PUT",
      headers: {
        Authorization: `Bearer ${getToken()}`,
      },
      body: fileData,
    })
      .then((r) => r.json())
      .then((r) => {
        setImage(r.message);
        setNotify({
          isOpen: true,
          message: "Successfully uploaded the cover!",
          type: "success",
        });
      });
  };

  return (
    <Grid>
      <Paper elevation={0} style={paperStyle}>
        <PopUpSingleBookAlert
          notify={notify}
          handleCloseNotification={handleCloseNotification}
        />
        <Grid align="center">
          <Typography variant="caption">Fill the form to login</Typography>
        </Grid>
        <Formik initialValues={book} onSubmit={onCreate}>
          {(props) => (
            <Form noValidate>
              <Field
                as={TextField}
                name="name"
                label="Name"
                fullWidth
                required
              />

              <Field
                as={TextField}
                name="author"
                label="Author"
                type="text"
                fullWidth
                required
              />
              <Field
                as={TextField}
                name="year"
                label="Year"
                type="number"
                fullWidth
                required
              />
              <Field
                as={TextField}
                name="genre"
                label="Genre"
                type="text"
                fullWidth
                required
              />
              <Field
                as={TextField}
                name="description"
                label="Description"
                type="text"
                fullWidth
                required
              />
              <Input
                as={TextField}
                name="coverUrl"
                label="hello"
                placeholder="Hello"
                type="file"
                style={{ fontFamily: "Orbitron" }}
                required
                onChange={(e) => setFile(e.target.files[0])}
              />

              <ButtonGroup disableElevation variant="outlined" color="primary">
                <Button
                  type="submit"
                  style={btnStyle}
                  variant="contained"
                  color="default"
                >
                  Create
                </Button>
                <Button
                  style={btnStyle}
                  variant="contained"
                  color="default"
                  onClick={onUpload}
                >
                  Upload
                </Button>
              </ButtonGroup>
            </Form>
          )}
        </Formik>
      </Paper>
    </Grid>
  );
};

export default CreateBook;
