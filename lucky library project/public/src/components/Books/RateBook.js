import Rating from "@material-ui/lab/Rating";
import Box from "@material-ui/core/Box";
import { useState } from "react";
import { BASE_URL } from "../../constants/constants";
import Button from "@material-ui/core/Button";
import { getToken } from "../../providers/authContext";
import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles({
  root:{
    display: "inline",
    height:'5%',
    width:'100%',
    justifyContent:'center',
    alignItems:'center',
  },
  box: {
    display:'grid',
    width:'100%',
    justifyContent:'center',
    alignItems:'center',
  },
  rating:{
    display:'flex',
    width:'100%',
    position:'relative',
    justifyContent:'center',
    alignItems:'center'
  },
  button: {
    display:'flex',
    fontSize: "1rem",
    backgroundColor: "rgba(0,0,0,0.4)",
    marginTop: 10,
    width: "300px",
    fontFamily: "Orbitron",
    fontWeight: "900",
    color: "orange",
    justifyContent:'center',
    alignItems:'center',
  },
  text:{
    display:'flex',
    width:'100%',
    position:'relative',
    justifyContent:'center',
    alignItems:'center'
  }
});

const RateBook = ({ book, setNotify, setRefreshReviews, refreshReviews }) => {
  const classes = useStyles();
  const [value, setValue] = useState(2);

  const submitRating = () => {
    const token = getToken();

    if (!token) {
      setNotify({
        isOpen: true,
        message: "You have to log in before rating the book",
        type: "error",
      });
      return;
    }

    if (!value) {
      setNotify({
        isOpen: true,
        message: "Enter valid rating",
        type: "error",
      });
      setValue("");
      return;
    }
    console.log(value);

    fetch(`${BASE_URL}/books/${book.id}/rate`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({
        rating: value,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data.error) {
          setNotify({
            isOpen: true,
            message: data.error,
            type: "error",
          });
          setValue("");
          return;
        }
        setValue("");
        setNotify({
          isOpen: true,
          message: "Rating created",
          type: "success",
        });
        setRefreshReviews(!refreshReviews);
      });
  };

  return (
    <div className={classes.root}>
      <h2 className={classes.text}>Rate Book</h2>
      <Box
        component="fieldset"
        mb={3}
        borderColor="transparent"
        className={classes.box}
      >
        <Rating
          name="simple-controlled"
          value={value}
          className={classes.rating}
          onChange={(event, newValue) => {
            setValue(newValue);
          }}
        />
        <Button
          color="primary"
          variant="contained"
          onClick={submitRating}
          className={classes.button}
        >
          Submit
        </Button>
      </Box>
    </div>
  );
};

export default RateBook;
