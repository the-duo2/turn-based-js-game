import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { BASE_URL } from "../../constants/constants";
import { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  img: {
    float: "left",
    paddingRight: "3%",
  },
  button: {
    fontSize: "1rem",
    backgroundColor: "rgba(0,0,0,0.4)",
    marginTop: 10,
    fontFamily: "Orbitron",
    fontWeight: "900",
    color: "orange",
  },
}));

const CreateReview = ({ book, setNotify, setRefreshReviews }) => {
  const classes = useStyles();
  const [review, setReview] = useState("");

  const postReview = () => {
    const token = localStorage.getItem("tokenFinal");

    if (!token) {
      setNotify({
        isOpen: true,
        message: "You have to log in before writing a review for book",
        type: "error",
      });
      return;
    }

    if (review.length < 10 || review.length > 210) {
      setNotify({
        isOpen: true,
        message: "Review must be between 10 and 210 characters",
        type: "error",
      });
      return;
    }

    fetch(`${BASE_URL}/books/${+book.id}/reviews`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({
        content: review,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data.error) {
          setNotify({
            isOpen: true,
            message: data.error,
            type: "error",
          });
          setReview("");
          return;
        }
        setNotify({
          isOpen: true,
          message: "Review successfully created",
          type: "success",
        });
        setReview("");

        setRefreshReviews();
      });
  };

  return (
    <>
      <form autocomplete="off">
        <TextField
          id="standard-basic"
          label="Write Review"
          value={review}
          onChange={(e) => setReview(e.target.value)}
        />
        <Button
          variant="contained"
          color="primary"
          size="small"
          className={classes.button}
          onClick={postReview}
        >
          Submit Review
        </Button>
      </form>
    </>
  );
};

export default CreateReview;
