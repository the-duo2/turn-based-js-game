import { useEffect, useState } from "react";
import { BASE_URL } from "../../constants/constants";
import SingleReview from "./SingleReview";

const ReviewsByBook = ({ book, setRefreshReviews, refreshReviews }) => {
  const [reviews, setReviews] = useState([]);

  useEffect(() => {
    fetch(`${BASE_URL}/books/${+book.id}/reviews`)
      .then((res) => res.json())
      .then((data) => {
        if (data.error) {
          setReviews([]);
          return;
        }
        setReviews(data);
        setRefreshReviews(!refreshReviews);
      });
  }, [refreshReviews]);

  return (
    <div>
      <h2>Reviews:</h2>
      {reviews.length === 0
        ? "No review yet"
        : reviews.map((review) => (
            <SingleReview
              key={review.id}
              review={review}
              book={book}
              setReviews={setReviews}
              setRefreshReviews={setRefreshReviews}
              refreshReviews={refreshReviews}
            />
          ))}
    </div>
  );
};

export default ReviewsByBook;
