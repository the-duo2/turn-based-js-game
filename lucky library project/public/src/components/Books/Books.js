import { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { useCoverCardMediaStyles } from "@mui-treasury/styles/cardMedia/cover";
import { BASE_URL } from "../../constants/constants";
import { Grid } from "@material-ui/core";
import SingleBook from "./SingleBook";
import BooksPage from "./BooksPage";
import SearchBar from "./SearchBar";
import Footer from "../Footer/Footer";
import BookGenre from "./BookGenre";

const useStyles = makeStyles((theme) => ({
  card: {
    borderRadius: "1rem",
    boxShadow: "none",
    position: "relative",
    width: 350,
    height: 500,
    "&:after": {
      content: '""',
      display: "block",
      position: "absolute",
      width: "100%",
      height: "64%",
      bottom: 0,
      zIndex: 1,
      background: "linear-gradient(to top, #000, rgba(0,0,0,0))",
    },
  },
  content: {
    position: "absolute",
    zIndex: 2,
    bottom: 0,
    width: "100%",
  },
  root: {
    flexGrow: 1,
    margin: "0 auto",
    alignItems: "center",
    width: 1500,
  },
  page: {
    marginBottom: "2%",
    float: "right",
  },
  whole: {
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover",
    backgroundColor: "black",
    filter: "brightness(90%)",
    opacity: "1",
    backgroundImage: `linear-gradient(to bottom, rgba(255, 146, 0, 1), rgba(0, 0, 0, 0.1)),
    url(${process.env.PUBLIC_URL + "/assets/blackwhite.jpg"})`,
  },
  h1: {
    textAlign: "center",
    marginTop: "0",
    fontSize: "3rem",
    fontFamily: "Orbitron",
    color: "#fff",
    paddingTop: "2%",
  },
  btn: {
    color: "#fff",
    fontWeight: "700",
    fontFamily: "Orbitron",
  },
  h1Height: {
    height: "50vh",
    margin: "0 auto",
    color: "#fff",
    fontWeight: "700",
    fontFamily: "Orbitron",
    fontSize:'4rem'
  },
}));

const Books = () => {
  const mediaStyles = useCoverCardMediaStyles({ bgPosition: "top" });
  const styles = useStyles();

  const [books, setBooks] = useState([]);
  const [isLoading, setIsLoading] = useState(null);
  const [error, setError] = useState(null);

  const [page, setPage] = useState("0");

  useEffect(() => {
    setIsLoading(true);

    fetch(`${BASE_URL}/books?page=${page}`)
      .then((res) => res.json())
      .then((data) => {
        if (!Array.isArray(data)) {
          throw new Error("error");
        }
        setBooks(data);
      })
      .catch((error) => setError(error))
      .finally(() => setIsLoading(false));
  }, [page]);

  const handlePage = (a) => setPage(a);

  return (
    <div className={styles.whole}>
      <div className={styles.root}>
        <h1 className={styles.h1}>All Available Books</h1>
        <SearchBar setBooks={setBooks} className={styles.searchBar}/>
        <BookGenre styles={styles} setBooks={setBooks} page={page}/>
        <BooksPage styles={styles} handlePage={handlePage} page={page} />

        <Grid container spacing={7}>
          {books.length === 0 ? (
            <h1 className={styles.h1Height}>No such books</h1>
          ) : (
            books.map((book) => (
              <SingleBook
                key={book.id}
                book={book}
                mediaStyles={mediaStyles}
                styles={styles}
              />
            ))
          )}
        </Grid>
        <Footer />
      </div>
    </div>
  );
};

export default Books;
