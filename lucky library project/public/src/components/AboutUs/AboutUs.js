import { makeStyles } from "@material-ui/core";
import ImageCard from "./ImageCard";
import martin from "../../constants/Static";
import useWindowPosition from "../../hooks/useWindowPosition";

const useStyles = makeStyles((theme) => ({
  root: {
    minHeight: "100vh",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    [theme.breakpoints.down("md")]: {
      flexDirection: "column",
    },
    marginTop: -100,
    marginBottom: -30,
  },
  text: {
    display: "grid",
    textAlign: "center",
    fontFamily: "Orbitron",
    fontSize: "3rem",
    color: "#fff",
    marginBottom: 0,
  },
}));
const AboutUs = () => {
  const checked = useWindowPosition("home");
  const classes = useStyles();
  return (
    <div className={classes.root2}>
      <h1 className={classes.text}>The team</h1>
      <div className={classes.root} id="place-to-visit">
        <ImageCard martin={martin[0]} checked={checked} />
        <ImageCard martin={martin[1]} checked={checked} />
      </div>
    </div>
  );
};

//style={{color:'orange'}}
export default AboutUs;
