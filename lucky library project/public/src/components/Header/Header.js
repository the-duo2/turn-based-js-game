import React from "react";
import { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Button, Toolbar } from "@material-ui/core";
import SimpleMenu from "../Dialog/SimpleMenu";
import SimpleDialog from "../Dialog/Dialog";
import { Link } from "react-router-dom";
import AccountMenu from "../Dialog/AccountMenu";

const useStyles = makeStyles((theme) => ({
  appbar: {
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover",
    backgroundColor: "rgba(255, 146, 0, 1)",
    filter: "brightness(90%)",
    display: "grid",
    opacity: "1",
  },
  appbarTitle: {
    flexGrow: "1",
    color: "#fff",
    position: "relative",
    marginRight: "700px",
    fontFamily: "Orbitron",
    fontSize: "2rem",
    fontWeight: "900",
    position: "relative",
  },
  appbarWrapper: {
    width: "95%",
    margin: "0 auto",
    background: "transparent",
    fontFamily: "Orbitron",
  },
  button: {
    fontFamily: "Orbitron",
    fontSize: "2rem",
    color: "#fff",
    borderStyle: "none",
    fontWeight: "900",
    display: "grid",
    position: "relative",
  },
}));

const Header = () => {
  const classes = useStyles();

  const [open, setOpen] = useState(false);
  const [token, setToken] = useState(localStorage.getItem("tokenFinal"));
  console.log(token);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = (value) => {
    setOpen(false);
  };

  return (
    <div className={classes.appbar}>
      <Toolbar className={classes.appbarWrapper}>
        <Button className={classes.appbarTitle} component={Link} to="/Home">
          M&M
        </Button>
        <SimpleMenu />
        {token ? (
          ""
        ) : (
          <div>
            <Button
              variant="outlined"
              color="default"
              onClick={handleClickOpen}
              className={classes.button}
            >
              Login
            </Button>
            <SimpleDialog open={open} onClose={handleClose} />
          </div>
        )}
        {token ? <AccountMenu /> : ""}
      </Toolbar>
    </div>
  );
};

export default Header;
