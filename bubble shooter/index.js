const canvas = document.querySelector("canvas");
const projects = [];
const enemies = [];
/**
ctx = context for the canvas
canvas context is basically the canvas api
drawing on the canvas and do whatever we need on the canvas
calling it c just to avoid bulkiness
specifying what kind of context - going to be a 2d game
 */

const c = canvas.getContext("2d");
console.log(c);

// This takes the full width and height of the window

canvas.width = innerWidth;
canvas.height = innerHeight;

console.log(canvas);

/*
this makes our player, it has a x and y for coordinates
a radius and color, it also has a draw function that creates it
on our canvas via begin-path and arc
 */
class Player {
  constructor(x, y, radius, color) {
    this.x = x;
    this.y = y;

    this.radius = radius;
    this.color = color;
  }
  /* We use draw to create the player circle */

  draw() {
    c.beginPath();
    c.arc(this.x, this.y, this.radius, 0, Math.PI * 2, false);
    c.fillStyle = this.color;
    c.fill();
  }
}

/**
this makes our player centered whatever the height and 
width of the screen are
 */

const x = canvas.width / 2;
const y = canvas.height / 2;

const player = new Player(x, y, 30, "blue");

player.draw();

function moveUp() {
  player.y -= 1;
  c.clearRect(0, 0, canvas.width, canvas.height);
  player.draw();
}
function moveDown() {
  player.y += 1;
  c.clearRect(0, 0, canvas.width, canvas.height);
  player.draw();
}
function moveLeft() {
  player.x -= 1;
  c.clearRect(0, 0, canvas.width, canvas.height);
  player.draw();
}
function moveRight() {
  player.x += 1;
  c.clearRect(0, 0, canvas.width, canvas.height);
  player.draw();
}

const controller = {
  w: { pressed: false, func: moveUp },
  s: { pressed: false, func: moveDown },
  a: { pressed: false, func: moveLeft },
  d: { pressed: false, func: moveRight },
};

document.addEventListener("keydown", (e) => {
  if (controller[e.key]) {
    controller[e.key].pressed = true;
  }
});
document.addEventListener("keyup", (e) => {
  if (controller[e.key]) {
    controller[e.key].pressed = false;
  }
});

const executeMoves = () => {
  Object.keys(controller).forEach((key) => {
    controller[key].pressed && controller[key].func();
  });
};

/* 
Creating a class for the projectiles, they are
basically the same, but they also have velocity, 
since they are going to be moving.
*/

class Projectile extends Player {
  constructor(x, y, radius, color, velocity) {
    super(x, y, radius, color);

    this.velocity = velocity;
  }
  /*
The update calls the draw function over and over and
updates its coordinates based on the velocity
*/

  update() {
    this.draw();
    this.x = this.x + this.velocity.x;
    this.y = this.y + this.velocity.y;
  }
}

/*
The enemy class is basically the same as the projectile class
but different - it goes from outside of the screen, on all sides
going towards the player
*/

class Enemy extends Projectile {
  constructor(x, y, radius, color, velocity) {
    super(x, y, radius, color, velocity);
  }
}

/* 
The animate function calls a
request animation frame that takes the animate 
function as a callback so it keeps looping over it, 
clearing the screen each time while drawing the player
and updating ( drawing ) the projectiles and enemies
*/

function animate() {
  requestAnimationFrame(animate);
  c.clearRect(0, 0, canvas.width, canvas.height);
  player.draw();
  executeMoves();

  projects.forEach((projectile) => {
    projectile.update();
  });
  enemies.forEach((enemy) => {
    enemy.update();

    projects.forEach((projectile) => {
      const dist = Math.hypot(projectile.x - enemy.x, projectile.y - enemy.y);
    
      if (dist - projectile.radius - enemy.radius < 1) {
        projects.splice(projects.indexOf(projectile), 1);
        enemies.splice(enemies.indexOf(enemy), 1);
      }
    });
  });
}

/* The spawn enemies function spawns an enemy every
1 second from all sides of the screen ( coming from outside of the screen )
towards the player.
*/

function spawnEnemies() {
  setInterval(() => {
    const radius = Math.random() * (60 - 10) + 10;
    let x;
    let y;
    if (Math.random() < 0.5) {
      x = Math.random() < 0.5 ? 0 - radius : canvas.width + radius;
      y = Math.random() * canvas.height;
    } else {
      x = Math.random() * canvas.width;
      y = Math.random() < 0.5 ? 0 - radius : canvas.height + radius;
    }

    const color = "green";

    const angle = Math.atan2(canvas.height / 2 - y, canvas.width / 2 - x);
    const velocity = {
      x: Math.cos(angle),
      y: Math.sin(angle),
    };

    enemies.push(new Enemy(x, y, radius, color, velocity));
  }, 1000);
}

/* The click event creates and pushes a projectile inside of the
projectile array, that we then use inside of the animate function
*/

window.addEventListener("click", (event) => {
  const angle = Math.atan2(event.clientY - player.y, event.clientX - player.x);

  const velocity = {
    x: Math.cos(angle) * 10,
    y: Math.sin(angle) * 10,
  };

  projects.push(new Projectile(player.x, player.y, 5, "red", velocity));
});

animate();
spawnEnemies();
