-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema playlist_generator
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema playlist_generator
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `playlist_generator` DEFAULT CHARACTER SET latin1 ;
USE `playlist_generator` ;

-- -----------------------------------------------------
-- Table `playlist_generator`.`artist`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `playlist_generator`.`artist` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `artist_id` INT(11) NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  `picture` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `playlist_generator`.`genre`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `playlist_generator`.`genre` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `genre_id` INT(11) NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `playlist_generator`.`playlist`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `playlist_generator`.`playlist` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(45) NOT NULL,
  `duration` INT(11) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `playlist_generator`.`album`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `playlist_generator`.`album` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `album_id` INT NOT NULL,
  `title` VARCHAR(255) NOT NULL,
  `picture` VARCHAR(255) NOT NULL,
  `genre_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `playlist_generator`.`track`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `playlist_generator`.`track` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `track_id` INT(11) NOT NULL,
  `title` VARCHAR(100) NOT NULL,
  `duration` INT(11) NOT NULL,
  `rank` INT(11) NOT NULL,
  `preview` VARCHAR(255) NOT NULL,
  `picture` VARCHAR(255) NOT NULL,
  `artist_id` INT(11) NOT NULL,
  `genre_id` INT(11) NOT NULL,
  `album_id` INT NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `playlist_generator`.`track_has_playlist`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `playlist_generator`.`track_has_playlist` (
  `track_id` INT(11) NOT NULL,
  `playlist_id` INT(11) NOT NULL,
  PRIMARY KEY (`track_id`, `playlist_id`),
  INDEX `fk_track_has_playlist_playlist1_idx` (`playlist_id` ASC) VISIBLE,
  INDEX `fk_track_has_playlist_track1_idx` (`track_id` ASC) VISIBLE,
  CONSTRAINT `fk_track_has_playlist_playlist1`
    FOREIGN KEY (`playlist_id`)
    REFERENCES `playlist_generator`.`playlist` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_track_has_playlist_track1`
    FOREIGN KEY (`track_id`)
    REFERENCES `playlist_generator`.`track` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

ALTER TABLE `playlist_generator`.`album` 
CHANGE COLUMN `title` `title` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL ;
ALTER TABLE `playlist_generator`.`artist` 
CHANGE COLUMN `name` `name` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL ;
ALTER TABLE `playlist_generator`.`track` 
CHANGE COLUMN `title` `title` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL ;
