import express from 'express';
import bcrypt from 'bcrypt';
import { createUser, validateUser, banUser, unbanUser, userLogout, getUserById, updateAvatar } from '../data/users.js';
import createToken from '../auth/create-token.js';
import validateUserBody from '../validators/create-user-validator.js';
import validateBody from '../middlewares/validate-body.js';
import { authMiddleware } from '../auth/auth-middleware.js';
import loggedGuard from '../middlewares/token-verification.js'; 
import multer from 'multer';
import path from 'path';


const storage = multer.diskStorage({

    destination(req, file, cb) {
      cb(null, 'avatars');
    },
    filename(req, file, cb) {
      // for book upload / update
      // 1 - check if req.user is admin
      // 2 - if admin find the book by bookId
      // 3 - if the book has a picture, return the picture name
      // 4 - otherwise create a new picture name
      const filename = Date.now() + path.extname(file.originalname);
  
      cb(null, filename);
    },
    });
  
    const upload = multer({ storage });

const usersController = express.Router();

usersController

// Avatar upload
  
  
    .put('/avatar', authMiddleware, upload.single('avatar'), async (req, res) => {
    const result = await updateAvatar(req.user.id, req.file.filename);

    console.log(result);
  
    res.json({ message: 'Done.' });
    })

    // Get user by id

    .get('/:id', async(req, res) => {
        try {
           const user = await getUserById(+req.params.id);
           res.send(user);
        } catch (error) {
            throw new Error(error);
        }
    })

    // User register

    .post('/register', validateBody('user', validateUserBody), async (req, res) => {
        const user = req.body;
        user.password = await bcrypt.hash(user.password, 10);
      try {
          const result = await createUser(user);
          res.status(200).json(result);
      } catch (error) {
          res.json({ error : error.message });
      }
      
    })

    // User login

    .post('/login', async (req, res) => {
        try {
            const user = await validateUser(req.body);
    
            if (await validateUser(req.body)) {
                const tokenFinal = createToken({
                    id: user.id,
                    username: user.username,
                    email: user.email,
                });
    
                res.status(200).json({ tokenFinal });
    
            } else {
                res.status(401).json({ error : 'Invalid credentials!'});
            }
        } catch(error) {
            res.status(400).json({ error : error.message });
        }
    })

    // User ban

    .delete('/:id', authMiddleware, loggedGuard, async (req, res) => {
        try {
            const result = await banUser(req);
            res.status(200).json(result);
        } catch (error) {
            res.status(400).json({ error : error.message });
        }
    })

    // User unban

    .put('/:id', authMiddleware, async (req, res) => {
        try {
            const result = await unbanUser(req);
            res.status(200).json(result);
        } catch (error) {
            res.status(400).json({ error : error.message });
        }
    })

    .post('/logout', authMiddleware, async (req, res) => {
        await userLogout(req.headers.authorization.replace('Bearer', ''));
        res.json({ message : 'done'});
    });

export default usersController;