import express from 'express';
import songsService from '../services/songs-service.js';
import songsData from '../data/track.js';
import validateGenre from '../middlewares/validate-genre.js';

const songsController = express.Router();

songsController
  // get all tracks
  .get('/tracks', async (req,res) => {
    try {
      const track = 'track';
      const tracks = await songsService.getAllByParam(songsData)(track, req.query);

      if (tracks.length === 0) {
        return res.status(200).json({
          msg: 'No tracks available',
        })
      }

      res.status(200).json(tracks);
    } catch (error) {
      res.status(400).json ({
        error: error.message,
      });
    }
  })

  //get tracks by duration
  .get('/tracks/duration', async (req, res) => {
    try {
      const tracks = await songsService.getAllTracksByDuration(songsData)(req.query);

      if (tracks.length === 0) {
        return res.status(200).json({
          msg: 'No tracks available',
        })
      }

      res.status(200).json(tracks);
    } catch (error) {
      res.status(400).json ({
        error: error.message,
      });
    }
  })

  // get Tracks by genre

  .get('/tracks/genre/:id',validateGenre, async (req, res) => {
    try {
      const genreId = req.params.id;
      const tracks = await songsService.getAllTracksByGenre(songsData)(genreId);

      if (tracks.length === 0) {
        return res.status(200).json({
          msg: 'No tracks available',
        })
      }

      res.status(200).json(tracks);
    } catch (error) {
      res.status(400).json ({
        error: error.message,
      });
    }
  })


  // get all albums
  .get('/albums', async (req,res) => {
    try {
      const album = 'album';
      const albums = await songsService.getAllByParam(songsData)(album, req.query);

      if (albums.length === 0) {
        return res.status(200).json({
          msg: 'No tracks available',
        })
      }

      res.status(200).json(albums);
    } catch (error) {
      res.status(400).json ({
        error: error.message,
      });
    }
  })

  // get all albums
  .get('/albums', async (req,res) => {
    try {
      const album = 'album';
      const albums = await songsService.getAllByParam(songsData)(album, req.query);

      if (albums.length === 0) {
        return res.status(200).json({
          msg: 'No tracks available',
        })
      }

      res.status(200).json(albums);
    } catch (error) {
      res.status(400).json ({
        error: error.message,
      });
    }
  })

  // get all genres
  .get('/genres', async (req,res) => {
    try {
      const genre = 'genre';
      const genres = await songsService.getAllByParam(songsData)(genre, req.query);

      if (genres.length === 0) {
        return res.status(200).json({
          msg: 'No tracks available',
        })
      }

      res.status(200).json(genres);
    } catch (error) {
      res.status(400).json ({
        error: error.message,
      });
    }
  })


export default songsController;