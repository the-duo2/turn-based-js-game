import errorStrings from '../common/error-strings.js';
// Validates wether the genre exists

const genres = [85, 116, 152, 165, 98, 106, 129];

export default async (req, res, next) => {
  try{
      if (isNaN(req.params.id)) {
        throw new Error('Parameters must be numbers!');
      }
      console.log(req.params.id)
      if (!genres.includes(+req.params.id)) {
        throw new Error(errorStrings['genre']['genre']);
      }
      await next();
  } catch (error) {
      res.status(400).json({ error : error.message });
  } 
};