export default {
    user: {
        username: 'Username should be between 5 and 15 characters, and should contain letters and numbers!',
        password: 'Password should be between 5 and 15 characters, and should contain letters and numbers!',
        email: 'Email should be of valid format!',
    },
    genre: {
        genre: 'Genre must be one of [85, 116, 152, 165, 98, 106, 129]',
    }
};