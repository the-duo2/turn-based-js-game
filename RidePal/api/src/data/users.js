import db from './pool.js';
import bcrypt from 'bcrypt';
import { userRole } from '../common/user-role.js';

export const createUser = async (user) => {

  // Checks if username already exists 

  const userNameChecker = await db.query('SELECT * FROM users u WHERE u.username = ?', [user.username]);
  if (userNameChecker[0]) {
    throw new Error('Username should be unique!');
  }

  // Checks if email already exists

  const emailChecker = await db.query('SELECT * FROM users u WHERE u.email = ?', [user.email]);
  if (emailChecker[0]) {
    throw new Error ('Email should be unique!');
  }

  // Inserts data into the users table

  const createUserSql = `
    INSERT INTO users (username, password, email, role)
    VALUES (?, ?, ?, ?)
  `;

  const result = await db.query(createUserSql, [user.username, user.password, user.email, userRole.Basic]);

  // Selects the user so we can return him as a response

  const selectedUser = `
    SELECT u.id, u.username, u.email, u.role
    FROM users u
    WHERE u.id = ?
  `;

  const createdUser = (await db.query(selectedUser, [result.insertId]))[0];

  return {
    success: true,
    response: `User created with ID: ${createdUser.id} and Username: ${createdUser.username}`,
  };
};

export const validateUser = async ({ username, password }) => {

  // Selects the user

  const userData = await db.query(`
  SELECT *
  FROM users u
  WHERE u.username = ?
  `, [username]);

  // Checks if username exists

  if (userData.length === 0) {
    throw new Error('Username does not exist!');
  }

  if (userData[0].role === 1) {
    return userData[0];
  }
  // Compares password
  
  if (await bcrypt.compare(password, userData[0].password)) {
    return userData[0];
  }

  return null;

};


export const banUser = async (request) => {

  const adminCheck = `select *
  from users u
  where u.id = ? and u.role = 1`;

  const selectUser = `select *
  from users u
  where u.id = ? and u.role = 0`;

  const updater = `
  update users
  set isBanned = 1
  where id = ?
  `;

  const admin = (await db.query(adminCheck, [+request.user.id]))[0];
  const user = (await db.query(selectUser, [+request.params.id]))[0];

  // Check if user is an admin, if he/she isnt throws an error
  console.log(admin);
  if (admin) {
    throw new Error('Unauthorized attempt!');
  }

  // Checks if the user exists, if not throws an error

  if (user) {
    throw new Error('Invalid user!');
  }

  await db.query(updater, [+request.params.id]);

  return { message : `User with ID: ${+request.params.id} has been banned!`};

};

export const unbanUser = async (request) => {

  const adminCheck = `select *
  from users u
  where u.id = ? and u.role = 1`;

  const selectUser = `select *
  from users u
  where u.id = ? and u.role = 0`;

  const updater = `
  update users
  set isBanned = 0
  where id = ?
  `;

  const admin = (await db.query(adminCheck, [+request.user.id]))[0];
  const user = (await db.query(selectUser, [+request.params.id]))[0];

  // Check if user is an admin, if he/she isnt throws an error

  if (admin) {
    throw new Error('Unauthorized attempt!');
  }

  // Checks if the user exists, if not throws an error

  if (user) {
    throw new Error('Invalid user!');
  }

  await db.query(updater, [+request.params.id]);

  return { message : `User with ID: ${+request.params.id} has been unbanned!`};

};

export const userLogout = async (token) => {

  // Inserts the token into the db
  
  const deleteTokenQuery = `
  INSERT INTO tokens (token)
  VALUES (?)
  `;

  const deletedToken = await db.query(deleteTokenQuery, [token]);

  return deletedToken;

};

export const getUserById = async (id) => (await db.query('SELECT email, id, isBanned, role ,avatarUrl, username FROM users u WHERE u.id = ?',[id]))[0];

export const updateAvatar = async(id, fileName) => {
  const sql = 'UPDATE users u SET u.avatarUrl = ? WHERE u.id = ?';

  const result = await db.query(sql, [fileName, id]);

  return {
    success: result.affectedRows === 1,
    response: {},
  };
};
