import db from './pool.js';

const getAll = async (param, skip, limit) => {
  const sql = `
    SELECT *
    FROM ${param}
    LIMIT ?
    OFFSET ? 
  `;

  return await db.query(sql, [limit, skip]);
};

const getTrackByDuration = async (duration1, duration2) => {
  const sql = `
    SELECT *
    FROM track
    WHERE duration > ? AND duration < ?
  `;

  return await db.query(sql, [duration1, duration2]);
};

const getTrackByGenre = async (genreId) => {
  const sql = `
    SELECT *
    FROM track
    WHERE genre_id = ?
  `;

  return await db.query(sql, [genreId]);
};

export default {
  getAll,
  getTrackByDuration,
  getTrackByGenre,
};