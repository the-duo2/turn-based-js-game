import fetch from 'node-fetch';
import { DEEZER_URL } from '../common/constants.js';
import db from './pool.js';

const genres = {
  85: 'Alternative',
  116: 'Rap',
  152: 'Rock',
  165: 'R&B',
  98: 'Classical',
  106: 'Electro',
  129: 'Jazz',
};

const artists = [];
const covers = [];

const addGenres = async () => {
  const sql = `
    INSERT INTO genre (genre_id,name)
    VALUES (?, ?)
  `;

  await Promise.all(Object.keys(genres).map(key => {
    db.query(sql, [key, genres[key]])
  }))
  console.log('End of addGenres');
};

const addArtists = async () => {
  const sql = `
    INSERT INTO artist (artist_id,name, picture)
    VALUES (?, ?, ?)
  `;

  await Promise.all(Object.keys(genres).map((key) => {
    fetch(`${DEEZER_URL}/genre/${key}/artists`)
      .then(res => res.json())
      .then(data => {
        const arr = data.data.slice(0,10);
        arr.forEach(el => artists.push([el.id, key]));
        arr.map(el => db.query(sql,[el.id, el.name, el.picture]));
      })
  }));
  console.log('End of addArtists');
};

const addAlbums = async () => {
  const sql = `
    INSERT INTO album (album_id, title, picture, genre_id)
    VALUES (?, ?, ?, ?)
  `;

  // console.log(artists)

  await Promise.all(artists.map(artist => {
      fetch(`${DEEZER_URL}/artist/${artist[0]}/albums?limit=5`)
        .then(res => res.json())
        .then(data => {
          // console.log(genre);
          data.data.forEach(el => covers.push([el.id, artist[0], el.cover, artist[1]]));
          data.data.map(el => db.query(sql, [el.id, el.title, el.cover, artist[1]]));
        });
  }));
  console.log('End of addAlbums');
};

const addTracks = async () => {
  const sql = `
    INSERT INTO track (track_id, title, duration, rank, preview, picture, artist_id, genre_id, album_id)
    VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)
  `;
  // console.log(covers)
  await Promise.all(covers.map((artist, index) => {
    setTimeout(() => {
      fetch(`${DEEZER_URL}/album/${artist[0]}/tracks?limit=10`)
        .then(res => res.json())
        .then(data => {
          // console.log(data);
          data.data.map(el => {
            // console.log(el);
            db.query(sql, [el.id, el.title, el.duration, el.rank, el.preview, artist[2], artist[1], artist[3], artist[0]]);
          })
      })
      if (index === (covers.length - 1)) {
        console.log('End of seed!');
      }
    },1000*index);
    
  }));
}

const populate = async () => {

  console.log('Loading seed...')
  await addGenres();
  await addArtists();
  setTimeout(async () => {
    await addAlbums();
  },5000);
  setTimeout(async () => {
    await addTracks();
  },10000);
};

populate();
