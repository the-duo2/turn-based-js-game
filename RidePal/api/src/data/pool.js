import dotenv from 'dotenv';
import mariadb from 'mariadb';

const config = dotenv.config().parsed;

const pool = mariadb.createPool(
    {
        port: config.DBPORT,
        database: config.DATABASE,
        user: config.USER,
        password: config.PASSWORD,
        host: config.HOST,
    },
);

export default pool;