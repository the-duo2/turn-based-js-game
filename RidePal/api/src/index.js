import express from 'express';
import helmet from 'helmet';
import cors from 'cors';
import dotenv from 'dotenv';
import passport from 'passport';
import jwtStrategy from './auth/strategy.js';
import usersController from './controllers/users-controller.js';
import songsController from './controllers/songs-controller.js';

const config = dotenv.config().parsed;

const PORT = config.PORT;
const app = express();

app.use(express.json());
app.use(helmet());
app.use(cors());

passport.use(jwtStrategy);
app.use(passport.initialize());

app.use('/users', usersController);
app.use('/songs', songsController);

app.listen(PORT, () => console.log(`Listening on port ${PORT}`));
