import passportJwt from 'passport-jwt';
import dotenv from 'dotenv';


const SECRET_KEY = dotenv.config().parsed.SECRET_KEY;

const options = {
    secretOrKey: SECRET_KEY, // what the secret key is
    jwtFromRequest: passportJwt.ExtractJwt.fromAuthHeaderAsBearerToken(), // where to extract the token from
};

// the payload will be injected from the decoded token
const jwtStrategy = new passportJwt.Strategy(options, async (payload, done) => {
    const user = {
        id: payload.id,
        username: payload.username,
    };

    // the user object will be injected in the request object and can be accessed as req.user in authenticated controllers
    done(null, user);
});

export default jwtStrategy;