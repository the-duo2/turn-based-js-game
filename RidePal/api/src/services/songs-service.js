
const getAllByParam = songData => {
  return async (param, queries) => {
    let { skip, limit } = queries;
    if (!skip || skip < 0) {
      skip = 0;
    }
    if (!limit || limit < 10) {
      limit = 10;
    }
    if (limit > 30) {
      limit = 30;
    }
    return songData.getAll(param, +skip, +limit);
  };
};

const getAllTracksByDuration = songData => {
  return async (queries) => {
    let { duration1, duration2 } = queries;
    if (!duration1 || duration1 < 0) {
      duration1 = 0;
    }
    if (!duration2) {
      duration2 = 5000;
    }
    
    return songData.getTrackByDuration(+duration1, +duration2);
  };
};

const getAllTracksByGenre = songData => {
  return async (genreId) => {
    
    return songData.getTrackByGenre(+genreId);
  }
}


export default {
  getAllByParam,
  getAllTracksByDuration,
  getAllTracksByGenre,
}